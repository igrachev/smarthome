package mysmarthome.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.Logbook;

@Configuration
public class TestConfig {

    @Bean
    public Logbook logbook() {
        return Logbook.create();
    }
}
