package mysmarthome;

import mysmarthome.services.database.DatabaseInitializer;
import org.junit.jupiter.api.Test;
import org.springframework.ai.vectorstore.SimpleVectorStore;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class MainAppServiceTest {

    @MockBean
    private DatabaseInitializer databaseInitializer;

    @MockBean
    private SimpleVectorStore simpleVectorStore;

    @Test
    void contextLoads(){}
}
