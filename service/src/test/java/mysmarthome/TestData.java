package mysmarthome;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import mysmarthome.models.*;
import mysmarthome.models.auth.AuthRequest;
import mysmarthome.models.auth.AuthUser;
import mysmarthome.models.auth.Role;

import java.util.List;

public final class TestData {

    public static final String AUTH_BASE_URI = "/auth/authenticate";

    public static final String DEVICE_BASE_URI = "/resources/devices";

    public static final String GROUP_BASE_URI = "/resources/groups";

    public static final String HOUSEHOLD_BASE_URI = "/resources/households";

    public static final String ROOM_BASE_URI = "/resources/rooms";

    public static final String SCENARIO_BASE_URI = "/resources/scenarios";

    public static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjbGllbnQiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiVVNFUiJ9XSwiaWF0IjoxNzIwMDgwOTM1LCJleHAiOjI2NjYxNjA5MzV9.LXGC3UlXO03S-J5sd9zRzbdh09ut1VFVVkya04Qtv9k";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    public static final String AUTH_BODY = """
            {
                "username": "client",
                "password": "password"
            }
            """;

    public static AuthRequest getAuthRequest() {
        return new AuthRequest("client", "password");
    }

    public static final String JSON_PATCH_STRING = """
            [
              {
                "op": "replace",
                "path": "/favourite",
                "value": true
              }
            ]
            """;

    public static String getDeviceRequest() {
        return String.format(DEVICE_REQUEST, "959b9232-963d-43dd-8f99-bf7c07d9f70b", "on");
    }

    public static String getDeviceRequestWithIncorrectInstance() {
        return String.format(DEVICE_REQUEST, "959b9232-963d-43dd-8f99-bf7c07d9f70b", "incorrectValue");
    }

    public static String getDeviceRequestWithInvalidUuid() {
        return String.format(DEVICE_REQUEST, "959b9232", "on");
    }

    private static final String DEVICE_REQUEST = """
            {
              "devices": [
                {
                  "id": "%s",
                  "actions": [
                    {
                      "type": "devices.capabilities.on_off",
                      "state": {
                        "instance": "%s",
                        "value": true
                      }
                    }
                  ]
                }
              ]
            }""";

    public static final String GROUP_REQUEST = """
            {
              "actions": [
                {
                  "type": "devices.capabilities.on_off",
                  "state": {
                    "instance": "on",
                    "value": true
                  }
                }
              ]
            }""";

    public static JsonPatch getJsonPatch() throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(JSON_PATCH_STRING, JsonPatch.class);
    }

    public static JsonNode getJsonNodeDevice() throws JsonProcessingException, JsonPatchException {
        JsonPatch jsonPatch = getJsonPatch();
        return jsonPatch.apply(OBJECT_MAPPER.convertValue(getDevice(), JsonNode.class));
    }

    public static JsonNode getJsonNodeGroup() throws JsonProcessingException, JsonPatchException {
        JsonPatch jsonPatch = getJsonPatch();
        return jsonPatch.apply(OBJECT_MAPPER.convertValue(getGroup(), JsonNode.class));
    }

    public static AuthUser getAuthUser() {
        return new AuthUser(1L, "client", "password", Role.USER);
    }

    public static Device getDevice() {
        Device device = new Device();
        device.setId("1");
        device.setName("name");
        device.setState("state");
        device.setFavourite(false);
        device.setCapabilities(List.of(new Capability()));
        return device;
    }

    public static DeviceDTO getDeviceDto(Device device) {
        return new DeviceDTO(
                device.getId(),
                device.getName(),
                device.getState(),
                device.getCapabilities()
        );
    }

    public static Group getGroup() {
        Group group = new Group();
        group.setId("1");
        group.setName("name");
        group.setState("state");
        group.setFavourite(false);
        return group;
    }

    public static Household getHousehold() {
        Household household = new Household();
        household.setId("1");
        household.setName("name");
        return household;
    }

    public static Room getRoom() {
        Room room = new Room();
        room.setId("1");
        room.setName("name");
        return room;
    }

    public static Scenario getScenario() {
        Scenario scenario = new Scenario();
        scenario.setId("1");
        scenario.setName("name");
        return scenario;
    }

    public static String getObectAsString(Object object) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getListOfObectsAsString(BaseModel baseModel) {
        try {
            return OBJECT_MAPPER.writeValueAsString(List.of(baseModel));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
