package mysmarthome.services.devicedtomapper;

import mysmarthome.TestData;
import mysmarthome.models.DeviceDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeviceDTOMapperTest {

    @Test
    public void testApply() {
        DeviceDTOMapper deviceDTOMapper = new DeviceDTOMapper();
        DeviceDTO expectedDeviceDTO = TestData.getDeviceDto(TestData.getDevice());
        DeviceDTO actualDeviceDTO = deviceDTOMapper.apply(TestData.getDevice());
        assertEquals(expectedDeviceDTO, actualDeviceDTO);
    }
}
