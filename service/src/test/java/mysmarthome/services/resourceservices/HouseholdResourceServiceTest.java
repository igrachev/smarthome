package mysmarthome.services.resourceservices;

import mysmarthome.TestData;
import mysmarthome.models.Household;
import mysmarthome.repositories.HouseholdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HouseholdResourceServiceTest {

    @InjectMocks
    HouseholdResourceService householdResourceService;

    @Mock
    private HouseholdRepository householdRepository;

    @Test
    void testGetHouseholdList() {
        Household expectedHousehold = TestData.getHousehold();
        when(householdRepository.findAll()).thenReturn(List.of(TestData.getHousehold()));
        List<Household> actualHouseholdList = householdResourceService.getHouseholdList();
        assertEquals(1, actualHouseholdList.size());
        assertEquals(expectedHousehold, actualHouseholdList.get(0));
    }
}
