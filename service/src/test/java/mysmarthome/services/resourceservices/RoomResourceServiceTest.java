package mysmarthome.services.resourceservices;

import mysmarthome.TestData;
import mysmarthome.models.Room;
import mysmarthome.repositories.RoomRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoomResourceServiceTest {

    @InjectMocks
    private RoomResourceService roomResourceService;

    @Mock
    private RoomRepository roomRepository;

    @Test
    void testGetRoomList() {
        Room expectedRoom = TestData.getRoom();
        when(roomRepository.findAll()).thenReturn(List.of(TestData.getRoom()));
        List<Room> actualRoomList = roomResourceService.getRoomList();
        assertEquals(1, actualRoomList.size());
        assertEquals(expectedRoom, actualRoomList.get(0));
    }

    @Test
    void testGetRoomById() {
        Room expectedRoom = TestData.getRoom();
        when(roomRepository.findById(any())).thenReturn(Optional.of(TestData.getRoom()));
        Room actualRoom = roomResourceService.getRoomById(any());
        assertEquals(expectedRoom, actualRoom);
    }

}
