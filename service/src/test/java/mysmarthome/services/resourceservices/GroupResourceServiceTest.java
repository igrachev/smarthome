package mysmarthome.services.resourceservices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import mysmarthome.TestData;
import mysmarthome.models.Group;
import mysmarthome.models.requestsfromclient.GroupRequest;
import mysmarthome.repositories.GroupRepository;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GroupResourceServiceTest {

    @InjectMocks
    private GroupResourceService groupResourceService;

    @Mock
    private HttpRequestSender httpRequestSender;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private GroupRepository groupRepository;

    @Test
    void testGetGroupList() {
        Group expectedGroup = TestData.getGroup();
        when(groupRepository.findAll()).thenReturn(List.of(TestData.getGroup()));
        List<Group> actualGroupList = groupResourceService.getGroupList();
        assertEquals(1, actualGroupList.size());
        assertEquals(expectedGroup, actualGroupList.get(0));
    }

    @Test
    void testGetGroupById() {
        Group expectedGroup = TestData.getGroup();
        when(groupRepository.findById(any())).thenReturn(Optional.of(TestData.getGroup()));
        Group actualGroup = groupResourceService.getGroupById(any());
        assertEquals(expectedGroup, actualGroup);
    }

    @Test
    void testTurnOnOffGroup() {
        GroupRequest body = new GroupRequest();
        String expectedUri = "/groups/1/actions";
        HttpMethod expectedMethod = HttpMethod.POST;
        groupResourceService.turnOnOffGroup("1", body);
        verify(httpRequestSender).sendHttpRequest(eq(expectedUri), eq(expectedMethod), eq(body));
    }

    @Test
    void testUpdateGroup() throws JsonPatchException, JsonProcessingException {
        Group expectedGroup = TestData.getGroup();
        expectedGroup.setFavourite(true);
        when(groupRepository.findById(any())).thenReturn(Optional.of(TestData.getGroup()));
        when(objectMapper.convertValue(any(), eq(JsonNode.class))).thenReturn(TestData.getJsonNodeGroup());
        when(objectMapper.treeToValue(any(), eq(Group.class))).thenReturn(expectedGroup);
        groupResourceService.updateGroup("1", TestData.getJsonPatch());
        verify(groupRepository).save(expectedGroup);
    }
}
