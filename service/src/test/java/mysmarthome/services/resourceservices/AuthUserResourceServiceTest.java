package mysmarthome.services.resourceservices;

import mysmarthome.TestData;
import mysmarthome.configuration.security.JwtUtils;
import mysmarthome.repositories.AuthUserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthUserResourceServiceTest {

    @InjectMocks
    private AuthUserResourceService authUserResourceService;

    @Mock
    private AuthUserRepository authUserRepository;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtUtils jwtUtils;

    @Test
    void authenticateUser() {
        when(authUserRepository.findByUsername(any())).thenReturn(Optional.of(TestData.getAuthUser()));
        when(jwtUtils.generateToken(any())).thenReturn(TestData.TOKEN);
        String result = authUserResourceService.authenticateUser(TestData.getAuthRequest());
        assertEquals(TestData.TOKEN, result);
    }

    @Test
    void authenticateUser_userNotFound_throwsUsernameNotFoundException() {
        when(authUserRepository.findByUsername(any())).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> authUserResourceService.authenticateUser(TestData.getAuthRequest()));
    }
}
