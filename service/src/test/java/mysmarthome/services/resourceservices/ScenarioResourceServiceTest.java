package mysmarthome.services.resourceservices;

import mysmarthome.TestData;
import mysmarthome.models.Scenario;
import mysmarthome.repositories.ScenarioRepository;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ScenarioResourceServiceTest {

    @InjectMocks
    private ScenarioResourceService scenarioResourceService;

    @Mock
    private ScenarioRepository scenarioRepository;

    @Mock
    private HttpRequestSender httpRequestSender;

    @Test
    void testGetScenarioList() {
        Scenario expectedScenario = TestData.getScenario();
        when(scenarioRepository.findAll()).thenReturn(List.of(TestData.getScenario()));
        List<Scenario> actualScenarioList = scenarioResourceService.getScenarioList();
        assertEquals(1, actualScenarioList.size());
        assertEquals(expectedScenario, actualScenarioList.get(0));
    }

    @Test
    void testTurnOnOffScenario() {
        String expectedUri = "/scenarios/1/actions";
        HttpMethod expectedMethod = HttpMethod.POST;
        scenarioResourceService.turnOnOffScenario("1");
        verify(httpRequestSender).sendHttpRequest(eq(expectedUri), eq(expectedMethod));
    }
}
