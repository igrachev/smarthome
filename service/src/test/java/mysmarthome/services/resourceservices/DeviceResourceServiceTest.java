package mysmarthome.services.resourceservices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import mysmarthome.TestData;
import mysmarthome.models.Device;
import mysmarthome.models.DeviceDTO;
import mysmarthome.models.requestsfromclient.DeviceRequest;
import mysmarthome.repositories.DeviceRepository;
import mysmarthome.services.devicedtomapper.DeviceDTOMapper;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DeviceResourceServiceTest {

    @InjectMocks
    private DeviceResourceService deviceResourceService;

    @Mock
    private DeviceRepository deviceRepository;

    @Mock
    private DeviceDTOMapper deviceDTOMapper;

    @Mock
    private HttpRequestSender httpRequestSender;

    @Mock
    private ObjectMapper objectMapper;

    @Test
    void testGetDeviceList() {
        Device expectedDevice = TestData.getDevice();
        when(deviceRepository.findAll()).thenReturn(List.of(TestData.getDevice()));
        List<Device> actualDeviceList = deviceResourceService.getDeviceList();
        assertEquals(1, actualDeviceList.size());
        assertEquals(expectedDevice, actualDeviceList.get(0));
    }

    @Test
    void testGetDeviceById() {
        Device expectedDevice = TestData.getDevice();
        when(deviceRepository.findById(any())).thenReturn(Optional.of(TestData.getDevice()));
        Device actualDevice = deviceResourceService.getDeviceById(any());
        assertEquals(expectedDevice, actualDevice);
    }

    @Test
    void testGetDeviceDtoById() {
        DeviceDTO expectedDeviceDTO = TestData.getDeviceDto(TestData.getDevice());
        when(deviceRepository.findById(any())).thenReturn(Optional.of(TestData.getDevice()));
        when(deviceDTOMapper.apply(any())).thenReturn(TestData.getDeviceDto(TestData.getDevice()));
        DeviceDTO actualDeviceDTO = deviceResourceService.getDeviceDtoById(anyString());
        assertEquals(expectedDeviceDTO, actualDeviceDTO);
    }

    @Test
    void testTurnOnOffDevice() {
        DeviceRequest body = new DeviceRequest();
        String expectedUri = "/devices/actions";
        HttpMethod expectedMethod = HttpMethod.POST;
        deviceResourceService.turnOnOffDevice(body);
        verify(httpRequestSender).sendHttpRequest(eq(expectedUri), eq(expectedMethod), eq(body));
    }

    @Test
    void testUpdateDevice() throws JsonProcessingException, JsonPatchException {
        Device expectedDevice = TestData.getDevice();
        expectedDevice.setFavourite(true);
        when(deviceRepository.findById(any())).thenReturn(Optional.of(TestData.getDevice()));
        when(objectMapper.convertValue(any(), eq(JsonNode.class))).thenReturn(TestData.getJsonNodeDevice());
        when(objectMapper.treeToValue(any(), eq(Device.class))).thenReturn(expectedDevice);
        deviceResourceService.updateDevice("1", TestData.getJsonPatch());
        verify(deviceRepository).save(eq(expectedDevice));
    }
}
