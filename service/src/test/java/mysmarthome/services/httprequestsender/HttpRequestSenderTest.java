package mysmarthome.services.httprequestsender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mysmarthome.TestData;
import mysmarthome.configuration.TestConfig;
import mysmarthome.models.Device;
import mysmarthome.models.requestsfromclient.DeviceRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(HttpRequestSender.class)
@Import(TestConfig.class)
public class HttpRequestSenderTest {

    @Autowired
    HttpRequestSender httpRequestSender;

    @Autowired
    MockRestServiceServer server;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void sendHttpRequestWithoutBody() throws JsonProcessingException {
        server.expect(requestTo("url/testUri"))
                .andRespond(withSuccess(objectMapper.writeValueAsString(TestData.getDevice()), MediaType.APPLICATION_JSON));
        Device actualResponse = httpRequestSender.sendHttpRequest("/testUri", HttpMethod.GET, Device.class);
        assertEquals(TestData.getDevice(), actualResponse);
    }

    @Test
    void sendHttpRequestWithoutBodyVoid() {
        server.expect(requestTo("url/testUri")).andRespond(withSuccess());
        ResponseEntity<Void> actualResponse = httpRequestSender.sendHttpRequest("/testUri", HttpMethod.POST);
        assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
        assertFalse(actualResponse.hasBody());
    }

    @Test
    void sendHttpRequestWithBodyVoid() {
        server.expect(requestTo("url/testUri")).andRespond(withSuccess());
        ResponseEntity<Void> actualResponse = httpRequestSender.sendHttpRequest("/testUri", HttpMethod.GET, new DeviceRequest());
        assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
        assertFalse(actualResponse.hasBody());
    }
}
