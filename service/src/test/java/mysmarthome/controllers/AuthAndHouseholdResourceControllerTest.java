package mysmarthome.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import mysmarthome.TestData;
import mysmarthome.configuration.security.JwtAuthFilter;
import mysmarthome.configuration.security.JwtUtils;
import mysmarthome.configuration.security.SecurityConfig;
import mysmarthome.models.auth.AuthResponse;
import mysmarthome.repositories.AuthUserRepository;
import mysmarthome.services.resourceservices.AuthUserResourceService;
import mysmarthome.services.resourceservices.HouseholdResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.Optional;

import static mysmarthome.TestData.AUTH_BASE_URI;
import static mysmarthome.TestData.HOUSEHOLD_BASE_URI;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({HouseholdResourceController.class, AuthController.class})
@Import({SecurityConfig.class, JwtAuthFilter.class, JwtUtils.class})
public class AuthAndHouseholdResourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HouseholdResourceService householdResourceService;

    @MockBean
    private AuthUserRepository authUserRepository;

    @MockBean
    private AuthUserResourceService authUserResourceService;

    private String bearerToken;

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(authUserRepository.findByUsername(any())).thenReturn(Optional.of(TestData.getAuthUser()));
        when(authUserResourceService.authenticateUser(TestData.getAuthRequest())).thenReturn(TestData.TOKEN);
        MvcResult result = mockMvc.perform(post(AUTH_BASE_URI)
                        .content(TestData.AUTH_BODY)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();
        AuthResponse authResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponse.class);
        bearerToken = "Bearer " + authResponse.getToken();
    }

    @Test
    void testRetrieveAll() throws Exception {
        when(householdResourceService.getHouseholdList()).thenReturn(List.of(TestData.getHousehold()));
        mockMvc.perform(get(HOUSEHOLD_BASE_URI).header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getListOfObectsAsString(TestData.getHousehold())));
    }

    @Test
    void testRetrieveAll_unauthorized() throws Exception {
        when(householdResourceService.getHouseholdList()).thenReturn(List.of(TestData.getHousehold()));
        mockMvc.perform(get(HOUSEHOLD_BASE_URI))
                .andExpect(status().isForbidden());
    }
}
