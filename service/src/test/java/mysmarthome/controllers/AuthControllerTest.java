package mysmarthome.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import mysmarthome.TestData;
import mysmarthome.configuration.security.JwtAuthFilter;
import mysmarthome.configuration.security.JwtUtils;
import mysmarthome.configuration.security.SecurityConfig;
import mysmarthome.models.auth.AuthResponse;
import mysmarthome.repositories.AuthUserRepository;
import mysmarthome.services.resourceservices.AuthUserResourceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static mysmarthome.TestData.AUTH_BASE_URI;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthController.class)
@Import({SecurityConfig.class, JwtAuthFilter.class, JwtUtils.class})
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthUserResourceService authService;

    @MockBean
    private AuthUserRepository authUserRepository;

    @Test
    void authenticate() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(authService.authenticateUser(any())).thenReturn(TestData.TOKEN);
        mockMvc.perform(post(AUTH_BASE_URI)
                        .content(TestData.AUTH_BODY)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                            AuthResponse authResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponse.class);
                            assertEquals(TestData.TOKEN, authResponse.getToken());
                        }
                );
    }
}
