package mysmarthome.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import mysmarthome.TestData;
import mysmarthome.configuration.security.JwtAuthFilter;
import mysmarthome.configuration.security.JwtUtils;
import mysmarthome.configuration.security.SecurityConfig;
import mysmarthome.models.auth.AuthResponse;
import mysmarthome.repositories.AuthUserRepository;
import mysmarthome.services.resourceservices.AuthUserResourceService;
import mysmarthome.services.resourceservices.GroupResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.Optional;

import static mysmarthome.TestData.*;
import static mysmarthome.exceptionhandling.ErrorMessages.MISSING_ATTRIBUTE;
import static mysmarthome.exceptionhandling.ErrorMessages.MISSING_BODY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({GroupResourceController.class, AuthController.class})
@Import({SecurityConfig.class, JwtAuthFilter.class, JwtUtils.class})
public class AuthAndGroupResourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GroupResourceService groupResourceService;

    @MockBean
    private AuthUserRepository authUserRepository;

    @MockBean
    private AuthUserResourceService authUserResourceService;

    private String bearerToken;

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(authUserRepository.findByUsername(any())).thenReturn(Optional.of(TestData.getAuthUser()));
        when(authUserResourceService.authenticateUser(TestData.getAuthRequest())).thenReturn(TestData.TOKEN);
        MvcResult result = mockMvc.perform(post(AUTH_BASE_URI)
                        .content(TestData.AUTH_BODY)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();
        AuthResponse authResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponse.class);
        bearerToken = "Bearer " + authResponse.getToken();
    }

    @Test
    void testRetrieveAll() throws Exception {
        when(groupResourceService.getGroupList()).thenReturn(List.of(TestData.getGroup()));
        mockMvc.perform(get(GROUP_BASE_URI).header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getListOfObectsAsString(TestData.getGroup())));
    }

    @Test
    void testRetrieveGroupById() throws Exception {
        when(groupResourceService.getGroupById(any())).thenReturn(TestData.getGroup());
        mockMvc.perform(get(GROUP_BASE_URI + "/1").header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getObectAsString(TestData.getGroup())));
    }

    @Test
    void testTurnOnOffGroup() throws Exception {
        when(groupResourceService.turnOnOffGroup(any(), any())).thenReturn(ResponseEntity.ok().build());
        mockMvc.perform(post(GROUP_BASE_URI + "/turnonoff/959b9232-963d-43dd-8f99-bf7c07d9f70b")
                        .header("Authorization", bearerToken)
                        .content(TestData.GROUP_REQUEST)
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void testTurnOnOffGroup_noBody() throws Exception {
        mockMvc.perform(post(GROUP_BASE_URI + "/turnonoff/959b9232-963d-43dd-8f99-bf7c07d9f70b")
                        .header("Authorization", bearerToken)
                        .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(MISSING_BODY));
    }

    @Test
    void testTurnOnOffGroup_noActionsAttribute() throws Exception {
        mockMvc.perform(post(GROUP_BASE_URI + "/turnonoff/959b9232-963d-43dd-8f99-bf7c07d9f70b")
                        .header("Authorization", bearerToken)
                        .content("{}")
                        .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(String.format(MISSING_ATTRIBUTE, "actions")));
    }

    @Test
    void testUpdateGroup() throws Exception {
        when(groupResourceService.updateGroup(any(), any())).thenReturn(TestData.getGroup());
        mockMvc.perform(
                        patch(GROUP_BASE_URI + "/1")
                                .header("Authorization", bearerToken)
                                .content(JSON_PATCH_STRING)
                                .contentType("application/json-patch+json"))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getObectAsString(TestData.getGroup())));
    }

    @Test
    void testRetrieveAll_unauthorized() throws Exception {
        when(groupResourceService.getGroupList()).thenReturn(List.of(TestData.getGroup()));
        mockMvc.perform(get(GROUP_BASE_URI))
                .andExpect(status().isForbidden());
    }
}
