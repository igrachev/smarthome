package mysmarthome.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import mysmarthome.TestData;
import mysmarthome.configuration.security.JwtAuthFilter;
import mysmarthome.configuration.security.JwtUtils;
import mysmarthome.configuration.security.SecurityConfig;
import mysmarthome.models.auth.AuthResponse;
import mysmarthome.repositories.AuthUserRepository;
import mysmarthome.services.resourceservices.AuthUserResourceService;
import mysmarthome.services.resourceservices.DeviceResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.Optional;

import static mysmarthome.TestData.*;
import static mysmarthome.exceptionhandling.ErrorMessages.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({DeviceResourceController.class, AuthController.class})
@Import({SecurityConfig.class, JwtAuthFilter.class, JwtUtils.class})
public class AuthAndDeviceResourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DeviceResourceService deviceResourceService;

    @MockBean
    private AuthUserRepository authUserRepository;

    @MockBean
    private AuthUserResourceService authUserResourceService;

    private String bearerToken;

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(authUserRepository.findByUsername(any())).thenReturn(Optional.of(TestData.getAuthUser()));
        when(authUserResourceService.authenticateUser(TestData.getAuthRequest())).thenReturn(TestData.TOKEN);
        MvcResult result = mockMvc.perform(post(AUTH_BASE_URI)
                        .content(TestData.AUTH_BODY)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();
        AuthResponse authResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponse.class);
        bearerToken = "Bearer " + authResponse.getToken();
    }

    @Test
    void testRetrieveAll() throws Exception {
        when(deviceResourceService.getDeviceList()).thenReturn(List.of(TestData.getDevice()));
        mockMvc.perform(get(DEVICE_BASE_URI).header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getListOfObectsAsString(TestData.getDevice())));
    }

    @Test
    void testRetrieveDeviceById() throws Exception {
        when(deviceResourceService.getDeviceById(any())).thenReturn(TestData.getDevice());
        mockMvc.perform(get(DEVICE_BASE_URI + "/1").header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getObectAsString(TestData.getDevice())));
    }

    @Test
    void testRetrieveDeviceDto() throws Exception {
        when(deviceResourceService.getDeviceDtoById(any())).thenReturn(TestData.getDeviceDto(TestData.getDevice()));
        mockMvc.perform(get(DEVICE_BASE_URI + "/dto/1").header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getObectAsString(TestData.getDeviceDto(TestData.getDevice()))));
    }

    @Test
    void testTurnOnOffDevice() throws Exception {
        when(deviceResourceService.turnOnOffDevice(any())).thenReturn(ResponseEntity.ok().build());
        mockMvc.perform(post(DEVICE_BASE_URI + "/turnonoff")
                        .header("Authorization", bearerToken)
                        .content(TestData.getDeviceRequest())
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void testTurnOffDevice_noBody() throws Exception {
        mockMvc.perform(post(DEVICE_BASE_URI + "/turnonoff")
                        .header("Authorization", bearerToken)
                        .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(MISSING_BODY));
    }

    @Test
    void testTurnOffDevice_noDevicesAttribute() throws Exception {
        mockMvc.perform(post(DEVICE_BASE_URI + "/turnonoff")
                        .header("Authorization", bearerToken)
                        .content("{}")
                        .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(String.format(MISSING_ATTRIBUTE, "devices")));
    }

    @Test
    void testTurnOffDevice_incorrectInstanceAttribute() throws Exception {
        mockMvc.perform(post(DEVICE_BASE_URI + "/turnonoff")
                        .header("Authorization", bearerToken)
                        .content(TestData.getDeviceRequestWithIncorrectInstance())
                        .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(String.format(INCORRECT_ATTRIBUTE, "devices[0].actions[0].state.instance")));
    }

    @Test
    void testTurnOffDevice_invalidUuidAttribute() throws Exception {
        mockMvc.perform(post(DEVICE_BASE_URI + "/turnonoff")
                        .header("Authorization", bearerToken)
                        .content(TestData.getDeviceRequestWithInvalidUuid())
                        .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(String.format(INVALID_UUID, "devices[0].id")));
    }

    @Test
    void testUpdateDevice() throws Exception {
        when(deviceResourceService.updateDevice(any(), any())).thenReturn(TestData.getDevice());
        mockMvc.perform(
                        patch(DEVICE_BASE_URI + "/1")
                                .header("Authorization", bearerToken)
                                .content(JSON_PATCH_STRING)
                                .contentType("application/json-patch+json"))
                .andExpect(status().isOk())
                .andExpect(content().string(TestData.getObectAsString(TestData.getDevice())));
    }

    @Test
    void testRetrieveAll_unauthorized() throws Exception {
        mockMvc.perform(get(DEVICE_BASE_URI))
                .andExpect(status().isForbidden());
    }
}
