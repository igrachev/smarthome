package mysmarthome.controllers;

import lombok.RequiredArgsConstructor;
import mysmarthome.models.smartscenario.SmartScenario;
import mysmarthome.services.chat.SmartScenarioService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/smartscenarios")
@RequiredArgsConstructor
public class SmartScenarioController {

    private final SmartScenarioService smartScenarioService;

    @GetMapping("")
    public List<SmartScenario> getAllById(@RequestParam String householdId) {
        return smartScenarioService.getAllByHouseholdId(householdId);
    }

    @PostMapping("/create")
    public SmartScenario createSmartScenario(@RequestParam String householdId, @RequestParam String roomId,
                                             @RequestParam String promt) {
        return smartScenarioService.createSmartScenario(householdId, roomId, promt);
    }

    @PostMapping("/activate")
    public SmartScenario activateSmartScenario(@RequestParam Long smartScenarioId) {
        return smartScenarioService.activateSmartScenario(smartScenarioId);
    }

    @PostMapping("/start")
    public SmartScenario startSmartScenario(@RequestParam Long smartScenarioId) {
        return smartScenarioService.startSmartScenario(smartScenarioId);
    }

    @PostMapping("/deactivate")
    public SmartScenario deactivateSmartScenario(@RequestParam Long smartScenarioId) {
        return smartScenarioService.deactivateSmartScenario(smartScenarioId);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteSmartScenario(@RequestParam Long smartScenarioId) {
        smartScenarioService.deleteSmartScenario(smartScenarioId);
        return ResponseEntity.ok().build();
    }
}
