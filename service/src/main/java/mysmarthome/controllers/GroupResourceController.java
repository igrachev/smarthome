package mysmarthome.controllers;

import com.github.fge.jsonpatch.JsonPatch;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import mysmarthome.models.Group;
import mysmarthome.models.requestsfromclient.GroupRequest;
import mysmarthome.services.resourceservices.GroupResourceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/resources/groups")
public class GroupResourceController {

    private final GroupResourceService groupResourceService;

    @GetMapping()
    public List<Group> retrieveAll() {
        return groupResourceService.getGroupList();
    }

    @GetMapping("/{id}")
    public Group retrieveGroupById(@PathVariable String id) {
        return groupResourceService.getGroupById(id);
    }

    @PostMapping("/turnonoff/{id}")
    public ResponseEntity<Void> turnOnOffGroup(@PathVariable String id, @Valid @RequestBody GroupRequest body) {
        return groupResourceService.turnOnOffGroup(id, body);
    }

    @PatchMapping(value = "/{id}", consumes = "application/json-patch+json")
    public Group updateGroup(@PathVariable String id, @RequestBody JsonPatch patch) {
        return groupResourceService.updateGroup(id, patch);
    }
}
