package mysmarthome.controllers;

import lombok.AllArgsConstructor;
import mysmarthome.models.Scenario;
import mysmarthome.services.resourceservices.ScenarioResourceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("/resources/scenarios")
public class ScenarioResourceController {

    private final ScenarioResourceService scenarioResourceService;

    @GetMapping
    public List<Scenario> retrieveAll() {
        return scenarioResourceService.getScenarioList();
    }

    @PostMapping("/activate/{scenarioId}")
    public ResponseEntity<Void> activateScenario(@PathVariable String scenarioId) {
        return scenarioResourceService.turnOnOffScenario(scenarioId);
    }
}
