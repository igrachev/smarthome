package mysmarthome.controllers;

import lombok.AllArgsConstructor;
import mysmarthome.models.Room;
import mysmarthome.services.resourceservices.RoomResourceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("/resources/rooms")
public class RoomResourceController {

    private final RoomResourceService roomResourceService;

    @GetMapping
    public List<Room> retrieveAll() {
        return roomResourceService.getRoomList();
    }

    @GetMapping("/all")
    public List<Room> retrieveAllByHouseholdId(@RequestParam String householdId) {
        return roomResourceService.getRoomListByHouseholdId(householdId);
    }

    @GetMapping("/{id}")
    public Room retrieveRoomById(@PathVariable String id) {
        return roomResourceService.getRoomById(id);
    }
}
