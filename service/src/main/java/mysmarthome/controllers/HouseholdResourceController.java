package mysmarthome.controllers;

import lombok.AllArgsConstructor;
import mysmarthome.models.Household;
import mysmarthome.services.resourceservices.HouseholdResourceService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("/resources/households")
public class HouseholdResourceController {

    private final HouseholdResourceService householdResourceService;

    @GetMapping()
    public List<Household> retrieveAll() {
        return householdResourceService.getHouseholdList();
    }
}
