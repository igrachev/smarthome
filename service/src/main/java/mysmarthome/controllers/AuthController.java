package mysmarthome.controllers;

import lombok.RequiredArgsConstructor;
import mysmarthome.models.auth.AuthRequest;
import mysmarthome.models.auth.AuthResponse;
import mysmarthome.services.resourceservices.AuthUserResourceService;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthUserResourceService authService;

    @PostMapping("/authenticate")
    public AuthResponse authenticate(@RequestBody AuthRequest authRequestBody) {
        String token = authService.authenticateUser(authRequestBody);
        return new AuthResponse(token);
    }
}
