package mysmarthome.controllers;

import lombok.RequiredArgsConstructor;
import mysmarthome.models.chat.AssistantResponse;
import mysmarthome.services.chat.ChatService;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/chat")
@RequiredArgsConstructor
public class ChatController {

    private final ChatService chatService;

    @GetMapping("/generate")
    public AssistantResponse generate(@RequestParam String householdId, @RequestParam String message) {
        return chatService.sendRequest(householdId, message);
    }

    @GetMapping("/analyse/device")
    public AssistantResponse analyseDevice(@RequestParam String householdId, @RequestParam String deviceId) {
        return chatService.analyseDevice(householdId, deviceId);
    }

    @GetMapping("/analyse/group")
    public AssistantResponse analyseGroup(@RequestParam String householdId, @RequestParam String groupId) {
        return chatService.analyseGroup(householdId, groupId);
    }
}
