package mysmarthome.controllers;

import com.github.fge.jsonpatch.JsonPatch;
import io.micrometer.core.annotation.Timed;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import mysmarthome.models.Device;
import mysmarthome.models.DeviceDTO;
import mysmarthome.models.requestsfromclient.DeviceRequest;
import mysmarthome.services.resourceservices.DeviceResourceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("/resources/devices")
public class DeviceResourceController {

    private final DeviceResourceService deviceResourceService;

    @Timed(value = "retrieve.all.devices.time", histogram = true)
    @GetMapping()
    public List<Device> retrieveAll() {
        return deviceResourceService.getDeviceList();
    }

    @GetMapping("/{id}")
    public Device retrieveDeviceById(@PathVariable String id) {
        return deviceResourceService.getDeviceById(id);
    }

    @GetMapping("/dto/{id}")
    public DeviceDTO retrieveDeviceDto(@PathVariable String id) {
        return deviceResourceService.getDeviceDtoById(id);
    }

    @PostMapping("/turnonoff")
    public ResponseEntity<Void> turnOnOffDevice(@Valid @RequestBody DeviceRequest body) {
        return deviceResourceService.turnOnOffDevice(body);
    }

    @PatchMapping(value = "/{id}", consumes = "application/json-patch+json")
    public Device updateDevice(@PathVariable String id, @RequestBody JsonPatch patch) {
        return deviceResourceService.updateDevice(id, patch);
    }
}
