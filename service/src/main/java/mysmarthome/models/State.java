package mysmarthome.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mysmarthome.configuration.StateDeserializerConfig;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
@JsonDeserialize(using = StateDeserializerConfig.class)
public class State {
  private String instance;
  private String value;
}
