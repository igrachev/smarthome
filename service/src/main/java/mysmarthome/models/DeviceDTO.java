package mysmarthome.models;

import java.util.List;

public record DeviceDTO(
        String id,
        String name,
        String state,
        List<Capability> capabilities) {
}
