package mysmarthome.models.bodyforrequesttoyaapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmartScenarioResponse {

    @JsonProperty(required = true, value = "reasoning_steps")
    private List<String> reasoningSteps;

    @JsonProperty(required = true, value ="answer")
    private ManageCapabilityBody manageCapabilityBody;
}
