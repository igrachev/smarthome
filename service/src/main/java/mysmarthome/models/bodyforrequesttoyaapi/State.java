package mysmarthome.models.bodyforrequesttoyaapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mysmarthome.configuration.StateForBodyDeserializerConfig;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(using = StateForBodyDeserializerConfig.class)
public class State {

    @JsonProperty(required = true, value = "instance")
    private String instance;

    @JsonProperty(required = true, value = "value")
    private Object value;
}
