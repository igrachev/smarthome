package mysmarthome.models.bodyforrequesttoyaapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    @JsonProperty(required = true, value = "id")
    private String id;

    @JsonProperty(required = true, value = "actions")
    private List<Action> actions;
}
