package mysmarthome.models.bodyforrequesttoyaapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Action {

    @JsonProperty(required = true, value = "type")
    private String type;

    @JsonProperty(required = true, value = "state")
    private State state;

    @JsonProperty(required = true, value = "comment")
    private String comment;
}
