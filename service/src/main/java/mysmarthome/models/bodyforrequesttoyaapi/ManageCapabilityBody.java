package mysmarthome.models.bodyforrequesttoyaapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManageCapabilityBody {

    @JsonProperty(required = true, value = "devices")
    private List<Device> devices;
}
