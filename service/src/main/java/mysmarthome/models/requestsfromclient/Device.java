package mysmarthome.models.requestsfromclient;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mysmarthome.exceptionhandling.uuidvalidation.NotNullAndValidUUID;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    @Valid
    @NotNull
    @NotNullAndValidUUID
    private String id;

    @Valid
    @NotNull
    private List<Action> actions;

}
