package mysmarthome.models.requestsfromclient;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Action {

    @Valid
    @NotNull
    private String type;

    @Valid
    @NotNull
    private State state;
}
