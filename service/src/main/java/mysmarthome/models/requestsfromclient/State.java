package mysmarthome.models.requestsfromclient;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class State {

    @Valid
    @NotNull
    @Pattern(regexp = "^(on|off)$")
    private String instance;

    @Valid
    @NotNull
    private boolean value;
}
