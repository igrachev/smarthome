package mysmarthome.models.auth;

public enum Role {
    USER,
    ADMIN
}
