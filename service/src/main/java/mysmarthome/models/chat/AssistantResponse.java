package mysmarthome.models.chat;

import com.fasterxml.jackson.annotation.JsonProperty;

public record AssistantResponse(@JsonProperty(required = true, value = "response") String response) {
}
