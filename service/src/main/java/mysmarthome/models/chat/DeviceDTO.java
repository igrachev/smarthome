package mysmarthome.models.chat;

import mysmarthome.models.Capability;

import java.util.List;

public record DeviceDTO(
        String id,
        String name,
        String type,
        List<String> aliases,
        List<Capability> capabilities) {
}
