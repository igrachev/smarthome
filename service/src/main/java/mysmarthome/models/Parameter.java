package mysmarthome.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_parameter")
@Entity
public class Parameter {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonIgnore
  private Long id;
  @Column
  private String split;
  @Column
  private String color_model;
  @Column
  private String instance;
  @Column
  private String unit;
  @ElementCollection
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<Mode> modes;
  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "range_id")
  private Range range;
  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "temperature_k_id")
  private TemperatureK temperature_k;
  @OneToMany(cascade = CascadeType.MERGE)
  @JoinColumn(name = "event_id")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<Event> events;

}
