package mysmarthome.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class Characteristic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column
    private String type;

    @Embedded
    private State state;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JsonProperty("parameters")
    private Parameter parameters;

    @JsonProperty("last_updated")
    private OffsetDateTime lastUpdated;
}
