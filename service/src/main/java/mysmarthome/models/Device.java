package mysmarthome.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_device")
@NamedEntityGraph(name = "Device.fetchCapabilities",
        attributeNodes = @NamedAttributeNode(value = "capabilities", subgraph = "capabilities-subgraph"),
        subgraphs = {
                @NamedSubgraph(
                        name = "capabilities-subgraph",
                        attributeNodes = @NamedAttributeNode("parameters")
                )
        }
)
public class Device extends BaseModel{
    @ElementCollection
    private List<String> aliases;

    @ElementCollection
    private List<String> smartScenarioNames;

    @ElementCollection
    @Column(name = "device_groups")
    private List<String> groups;

    @Column
    @JsonProperty("household_id")
    private String householdId;

    @Column
    private String room;

    @Column
    private String type;

    @Column
    private String state;

    @Column
    private boolean favourite;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinColumn(name = "device_capabilities_id")
    private List<Capability> capabilities;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinColumn(name = "device_properties_id")
    private List<Property> properties;

    public Device setDeviceHousehold_id(String household_id) {
        this.setHouseholdId(household_id);
        return this;
    }

    public Device setDeviceFavourite(boolean favourite) {
        this.setFavourite(favourite);
        return this;
    }

    public Device setSmartScenarioNames(List<String> smartScenarioNames) {
        this.smartScenarioNames = smartScenarioNames;
        return this;
    }

    public void setSmartScenarioName(String smartScenarioName) {
        if(this.smartScenarioNames == null) {
            this.smartScenarioNames = Arrays.asList(smartScenarioName);
        } else if (!this.smartScenarioNames.contains(smartScenarioName)) {
            this.smartScenarioNames.add(smartScenarioName);
        }
    }

    public void removeSmartScenarioName(String smartScenarioName) {
        if(this.smartScenarioNames != null) {
            this.smartScenarioNames.remove(smartScenarioName);
        }
    }
}


