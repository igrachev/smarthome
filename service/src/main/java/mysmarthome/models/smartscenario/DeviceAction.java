package mysmarthome.models.smartscenario;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mysmarthome.configuration.StateDeserializerConfig;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(using = StateDeserializerConfig.class)
@Embeddable
public class DeviceAction {

    private String device;

    private String instance;

    private String value;

    private String comment;
}
