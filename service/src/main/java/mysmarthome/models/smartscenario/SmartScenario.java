package mysmarthome.models.smartscenario;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_smart_scenario")
public class SmartScenario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column(length = 2000)
    private String prompt;

    @Column
    private String roomId;

    @Column
    private String householdId;

    @Column
    private boolean active;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<SmartScenarioAction> actions = new ArrayList<>();

    public void setAction(SmartScenarioAction action) {
        this.actions.add(action);
    }

    public SmartScenario(String name, String prompt, String roomId, String householdId, boolean active, List<SmartScenarioAction> actions) {
        this.name = name;
        this.prompt = prompt;
        this.roomId = roomId;
        this.householdId = householdId;
        this.active = active;
        this.actions = actions;
    }

    public SmartScenario(String name, String prompt, String roomId, String householdId, boolean active) {
        this.name = name;
        this.prompt = prompt;
        this.roomId = roomId;
        this.householdId = householdId;
        this.active = active;
    }
}
