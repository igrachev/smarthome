package mysmarthome.models.smartscenario;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_smart_scenario_action")
public class SmartScenarioAction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private OffsetDateTime date;

    @Column(length = 1500)
    private String comment;

    @ElementCollection
    private List<DeviceAction> deviceActions;

    public SmartScenarioAction(OffsetDateTime date, String comment, List<DeviceAction> deviceActions) {
        this.date = date;
        this.comment = comment;
        this.deviceActions = deviceActions;
    }

}
