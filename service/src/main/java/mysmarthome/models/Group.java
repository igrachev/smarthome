package mysmarthome.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import mysmarthome.models.responses.GroupResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_group")
@NamedEntityGraph(name = "Group.fetchCapabilities",
        attributeNodes = @NamedAttributeNode(value = "capabilities", subgraph = "capabilities-subgraph"),
        subgraphs = {
                @NamedSubgraph(
                        name = "capabilities-subgraph",
                        attributeNodes = @NamedAttributeNode("parameters")
                )
        }
)
public class Group extends BaseModel{
    @ElementCollection
    private List<String> aliases;
    @Column
    @JsonProperty("household_id")
    private String householdId;
    @Column
    private String type;
    @Column
    private String state;
    @Column
    private boolean favourite;
    @ElementCollection
    private List<String> devices;
    @OneToMany(cascade = CascadeType.MERGE)
    @JoinColumn(name = "group_capabilities_id")
    private List<Capability> capabilities;

    public Group(String id, String name, List<String> aliases, String householdId, String type, String state,
                 boolean favourite, List<String> devices, List<Capability> capabilities) {
        super(id, name);
        this.aliases = aliases;
        this.householdId = householdId;
        this.type = type;
        this.state = state;
        this.favourite = favourite;
        this.devices = devices;
        this.capabilities = capabilities;
    }

    public Group setGroupFavourite(boolean favourite) {
        this.setFavourite(favourite);
        return this;
    }

    public Group setGroupHouseholdId(String householdId) {
        this.setHouseholdId(householdId);
        return this;
    }

    public Group setGroupDevices(List<DeviceInGroup> devicesInGroup) {
        if (devicesInGroup != null && !devicesInGroup.isEmpty()) {
            List<String> devicesToSet = new ArrayList<>();
            devicesInGroup.forEach(device -> devicesToSet.add(device.getId()));
            this.setDevices(devicesToSet);
        } else {
            this.setDevices(Collections.emptyList());
        }
        return this;
    }

    public static Group of(GroupResponse groupResponse) {
        return new Group(
                groupResponse.id(),
                groupResponse.name(),
                groupResponse.aliases(),
                groupResponse.household_id(),
                groupResponse.type(),
                groupResponse.state(),
                groupResponse.favourite(),
                null,
                groupResponse.capabilities()
        );
    }

}
