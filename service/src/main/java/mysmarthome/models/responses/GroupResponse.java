package mysmarthome.models.responses;

import mysmarthome.models.Capability;
import mysmarthome.models.DeviceInGroup;

import java.util.List;

public record GroupResponse(String id,
                            String name,
                            List<String> aliases,
                            String household_id,
                            String type,
                            String state,
                            boolean favourite,
                            List<DeviceInGroup> devices,
                            List<Capability> capabilities) {
}
