package mysmarthome.models.responses;

import mysmarthome.models.*;

import java.util.List;

public record AllInfoResponse(String status,
                              String request_id,
                              List<Room> rooms,
                              List<Group> groups,
                              List<Device> devices,
                              List<Scenario> scenarios,
                              List<Household> households) {
}
