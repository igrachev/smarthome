package mysmarthome.models.weather;

public record Condition(String text) {
}
