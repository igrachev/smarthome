package mysmarthome.models.weather;

public record Current(Double temp_c, Double wind_kph, Integer humidity, Condition condition) {
}
