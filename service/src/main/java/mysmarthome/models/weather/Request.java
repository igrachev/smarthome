package mysmarthome.models.weather;

public record Request(String city) {
}
