package mysmarthome;

import mysmarthome.configuration.CurrentLocationProperties;
import mysmarthome.configuration.WeatherConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({WeatherConfigProperties.class, CurrentLocationProperties.class})
public class MainAppService {

    public static void main(String[] args) {
        SpringApplication.run(MainAppService.class, args);
    }

}
