package mysmarthome.configuration;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import mysmarthome.models.State;

import java.io.IOException;

public class StateDeserializerConfig extends JsonDeserializer<State> {
    @Override
    public State deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String instance  = node.get("instance").asText();
        String value = node.get("value").asText();
        return new State(instance, value);
    }
}
