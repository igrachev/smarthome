package mysmarthome.configuration;

import mysmarthome.models.bodyforrequesttoyaapi.State;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;


import java.io.IOException;

public class StateForBodyDeserializerConfig extends JsonDeserializer<State> {
    @Override
    public State deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String instance = node.get("instance").asText();
        JsonNode valueNode = node.get("value");
        Object value = switch (instance) {
            case "temperature" -> valueNode.isTextual() ? Float.parseFloat(valueNode.asText()) : valueNode.floatValue();
            case "brightness", "temperature_k" ->
                    valueNode.isTextual() ? Integer.parseInt(valueNode.asText()) : valueNode.intValue();
            case "on" -> valueNode.isTextual() ? Boolean.parseBoolean(valueNode.asText()) : valueNode.booleanValue();
            case "open" -> valueNode.asText().matches("\\d+") ? Integer.parseInt(valueNode.asText()) : valueNode.asText();
            default -> valueNode.asText();
        };
        return new State(instance, value);
    }
}
