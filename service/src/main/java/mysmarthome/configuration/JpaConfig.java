package mysmarthome.configuration;

import mysmarthome.repositories.CustomJpaRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = "mysmarthome.repositories",
        repositoryBaseClass = CustomJpaRepository.class)
public class JpaConfig {
}
