package mysmarthome.configuration.chatconfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.chat.client.advisor.QuestionAnswerAdvisor;
import org.springframework.ai.chat.client.advisor.api.AdvisedRequest;
import org.springframework.ai.chat.client.advisor.api.AdvisedResponse;
import org.springframework.ai.chat.client.advisor.api.CallAroundAdvisor;
import org.springframework.ai.chat.client.advisor.api.CallAroundAdvisorChain;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class AdvisorConfig {

    @Bean
    public QuestionAnswerAdvisor questionAnswerAdvisor(VectorStore vectorStore) {
        return QuestionAnswerAdvisor.builder(vectorStore).withOrder(2).build();
    }

    @Bean
    public CallAroundAdvisor loggingAdvisor() {
        return new CallAroundAdvisor() {

            @Override
            public AdvisedResponse aroundCall(AdvisedRequest advisedRequest, CallAroundAdvisorChain chain) {
                log.info("LLM Request, \n* User text: {} \n* User params: {} \n* System text: {} \n* System params: {}",
                        advisedRequest.userText(),
                        advisedRequest.userParams(),
                        advisedRequest.systemText(),
                        advisedRequest.systemParams());
                AdvisedResponse advisedResponse = chain.nextAroundCall(advisedRequest);
                log.info("LLM Response: {}", advisedResponse.response().getResult().getOutput().getContent());
                return advisedResponse;
            }

            @Override
            public String getName() {
                return "LoggingAdvisor";
            }

            @Override
            public int getOrder() {
                return 3;
            }
        };
    }

    @Bean
    public CallAroundAdvisor reReadingAdvisor() {
        return new CallAroundAdvisor() {

            private AdvisedRequest before(AdvisedRequest advisedRequest) {
                Map<String, Object> advisedUserParams = new HashMap<>(advisedRequest.userParams());
                advisedUserParams.put("re2_input_query", advisedRequest.userText());
                return AdvisedRequest.from(advisedRequest)
                        .withUserText("""
                                {re2_input_query}
                                Read the question again: {re2_input_query}
                                """)
                        .withUserParams(advisedUserParams)
                        .build();
            }

            @Override
            public AdvisedResponse aroundCall(AdvisedRequest advisedRequest, CallAroundAdvisorChain chain) {
                return chain.nextAroundCall(this.before(advisedRequest));
            }

            @Override
            public String getName() {
                return "ReReadingAdvisor";
            }

            @Override
            public int getOrder() {
                return 1;
            }
        };
    }

}
