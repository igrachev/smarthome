package mysmarthome.configuration.chatconfig;

import lombok.RequiredArgsConstructor;
import mysmarthome.configuration.CurrentLocationProperties;
import mysmarthome.configuration.WeatherConfigProperties;
import mysmarthome.models.weather.Request;
import mysmarthome.models.weather.Response;
import mysmarthome.repositories.DeviceRepository;
import mysmarthome.services.chat.functioncallingservices.MotionDetectorInLastGivenMinutesService;
import mysmarthome.services.chat.functioncallingservices.WeatherService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;

import java.util.function.Function;

@Configuration
@RequiredArgsConstructor
public class FunctionConfig {

    private final WeatherConfigProperties weatherConfigProperties;

    private final CurrentLocationProperties currentLocationProperties;

    private final DeviceRepository deviceRepository;

    @Bean
    @Description("Get the current weather conditions for the given city like temperature, humidity, wind speed and general weather condition info like sunny, cloudy etc.")
    public Function<Request, Response> currentWeatherFunction() {
        return new WeatherService(weatherConfigProperties);
    }

    @Bean
    @Description("Determines if motion was detected in the given room in the last given minutes.")
    public Function<MotionDetectorInLastGivenMinutesService.Request, MotionDetectorInLastGivenMinutesService.Response> motionDetectorInLastGivenMinutesFunction() {
        return new MotionDetectorInLastGivenMinutesService(deviceRepository, currentLocationProperties);
    }
}
