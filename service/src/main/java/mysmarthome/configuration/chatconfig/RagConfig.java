package mysmarthome.configuration.chatconfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.document.Document;
import org.springframework.ai.embedding.EmbeddingModel;
import org.springframework.ai.reader.ExtractedTextFormatter;
import org.springframework.ai.reader.pdf.PagePdfDocumentReader;
import org.springframework.ai.reader.pdf.config.PdfDocumentReaderConfig;
import org.springframework.ai.transformer.splitter.TokenTextSplitter;
import org.springframework.ai.vectorstore.SimpleVectorStore;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.File;
import java.util.List;

@Configuration
@Slf4j
public class RagConfig {

    @Value("classpath:docs/Managing_device_capabilities.pdf")
    private Resource pdfResourceDocument;

    @Bean
    public SimpleVectorStore simpleVectorStore(@Qualifier("openAiEmbeddingModel") EmbeddingModel embeddingModel) {
        SimpleVectorStore simpleVectorStore = new SimpleVectorStore(embeddingModel);
        File vectorStoreFile = getFile();
        if (vectorStoreFile.exists()) {
            log.info("Vector store file exists, loading from file");
            simpleVectorStore.load(vectorStoreFile);
        } else {
            log.info("Vector store file does not exist, creating from pdf");
            var config = PdfDocumentReaderConfig.builder()
                    .withPageExtractedTextFormatter(new ExtractedTextFormatter.Builder()
                            .withNumberOfBottomTextLinesToDelete(0)
                            .withNumberOfTopPagesToSkipBeforeDelete(0)
                            .build())
                    .withPagesPerDocument(1)
                    .build();
            var pdfReader = new PagePdfDocumentReader(pdfResourceDocument, config);
            List<Document> documents = pdfReader.get();
            var textSplitter = new TokenTextSplitter();
            List<Document> splitDocuments = textSplitter.apply(documents);
            simpleVectorStore.add(splitDocuments);
            simpleVectorStore.save(vectorStoreFile);
        }
        return simpleVectorStore;
    }

    private File getFile() {
        try {
            File directory = new File("service/src/main/resources/data");
            if (!directory.exists()) {
                directory.mkdirs();
            }
            return new File(directory, "vectorstore.json");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
