package mysmarthome.configuration.chatconfig.chatclientbuilderconfig;

import org.springframework.ai.chat.client.ChatClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

@Configuration
@Profile("!local")
public class NonLocalChatClientBuilderConfig {

    @Bean
    @Scope("prototype")
    ChatClient.Builder configurableChatClientBuilder(ChatClient.Builder openAiChatClientBuilder) {
        return openAiChatClientBuilder;
    }
}
