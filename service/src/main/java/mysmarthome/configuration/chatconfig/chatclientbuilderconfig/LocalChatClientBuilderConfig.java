package mysmarthome.configuration.chatconfig.chatclientbuilderconfig;

import org.springframework.ai.autoconfigure.chat.client.ChatClientBuilderConfigurer;
import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.model.ChatModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

@Configuration
@Profile("local")
public class LocalChatClientBuilderConfig {

    @Bean
    @Scope("prototype")
    ChatClient.Builder configurableChatClientBuilder(ChatClientBuilderConfigurer chatClientBuilderConfigurer,
                                                     @Qualifier("ollamaChatModel") ChatModel chatModel) {
        ChatClient.Builder builder = ChatClient.builder(chatModel);
        return chatClientBuilderConfigurer.configure(builder);
    }
}
