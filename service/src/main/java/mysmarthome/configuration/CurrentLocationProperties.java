package mysmarthome.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(value = "current-location")
public record CurrentLocationProperties(String city) {
}
