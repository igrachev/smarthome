package mysmarthome.exceptionhandling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        String messageBody = ErrorMessages.INVALID_STRUCTURE;
        String errorMessage = exception.getMessage();
        if (errorMessage.contains("rejected value [null]")) {
            messageBody = getMessageBody(errorMessage, messageBody, ErrorMessages.MISSING_ATTRIBUTE);
        } else if (errorMessage.contains("rejected value") && errorMessage.contains("Pattern")) {
            messageBody = getMessageBody(errorMessage, messageBody, ErrorMessages.INCORRECT_ATTRIBUTE);
        } else if (errorMessage.contains("rejected value") && errorMessage.contains("Invalid UUID")) {
            messageBody = getMessageBody(errorMessage, messageBody, ErrorMessages.INVALID_UUID);
        }
        return new ResponseEntity<>(messageBody, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleHttpMessageNotReadableException(HttpMessageNotReadableException exception) {
        String messageBody = ErrorMessages.INVALID_STRUCTURE;
        String errorMessage = exception.getMessage();
        if (errorMessage.contains("Required request body is missing")) {
            messageBody = ErrorMessages.MISSING_BODY;
        }
        return new ResponseEntity<>(messageBody, HttpStatus.BAD_REQUEST);
    }

    private String getMessageBody(String errorMessage, String messageBody, String errorMessageTemplate) {
        Matcher matcher = Pattern.compile("on field \'([\\w\\[\\].]+)\'").matcher(errorMessage);
        while (matcher.find()) {
            messageBody = String.format(errorMessageTemplate, matcher.group(1));
        }
        return messageBody;
    }
}
