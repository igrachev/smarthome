package mysmarthome.exceptionhandling.uuidvalidation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.UUID;

public class UUIDValidator implements ConstraintValidator<NotNullAndValidUUID, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("rejected value [null]").addConstraintViolation();
            return false;
        }
        try {
            UUID.fromString(value);
            return true;
        } catch (IllegalArgumentException exception) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("Invalid UUID").addConstraintViolation();
            return false;
        }
    }
}
