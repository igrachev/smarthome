package mysmarthome.exceptionhandling;

public class ErrorMessages {

    public static final String INVALID_STRUCTURE = "Invalid structure";

    public static final String MISSING_BODY = "Request body is missing";

    public static final String MISSING_ATTRIBUTE = "%s attribute is missing";

    public static final String INCORRECT_ATTRIBUTE = "%s attribute is incorrect";

    public static final String INVALID_UUID = "Invalid UUID in %s attribute";
}
