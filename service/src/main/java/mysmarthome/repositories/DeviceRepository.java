package mysmarthome.repositories;

import mysmarthome.models.Device;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface DeviceRepository extends JpaRepository<Device, String> {

    @Override
    @EntityGraph("Device.fetchCapabilities")
    List<Device> findAll();

    List<Device> findAllByHouseholdId(String householdId);

    List<Device> findAllByRoom(String roomId);

    Device findByName(String name);
}
