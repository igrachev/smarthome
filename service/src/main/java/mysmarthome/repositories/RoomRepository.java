package mysmarthome.repositories;

import mysmarthome.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface RoomRepository extends JpaRepository<Room, String> {

    List<Room> findAllByHouseholdId(String householdId);
}
