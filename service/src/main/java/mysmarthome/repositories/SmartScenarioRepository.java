package mysmarthome.repositories;

import mysmarthome.models.smartscenario.SmartScenario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SmartScenarioRepository extends JpaRepository<SmartScenario, Long> {

    List<SmartScenario> findAllByHouseholdId(String householdId);
}
