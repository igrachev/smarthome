package mysmarthome.repositories;

import mysmarthome.models.Scenario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface ScenarioRepository extends JpaRepository<Scenario, String> {
}
