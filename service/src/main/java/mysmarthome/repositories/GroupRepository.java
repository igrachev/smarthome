package mysmarthome.repositories;

import mysmarthome.models.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface GroupRepository extends JpaRepository<Group, String> {
    @Override
    @EntityGraph("Group.fetchCapabilities")
    List<Group> findAll();

    List<Group> findAllByHouseholdId(String householdId);
}
