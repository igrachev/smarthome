package mysmarthome.repositories;

import mysmarthome.models.Household;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface HouseholdRepository extends JpaRepository<Household, String> {
}
