package mysmarthome.services.resourceservices;

import lombok.AllArgsConstructor;
import mysmarthome.models.Room;
import mysmarthome.repositories.RoomRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@AllArgsConstructor
@Service
public class RoomResourceService {

    private final RoomRepository roomRepository;

    public List<Room> getRoomList() {
        return roomRepository.findAll();
    }

    public List<Room> getRoomListByHouseholdId(String householdId) {
        return roomRepository.findAllByHouseholdId(householdId);
    }

    public Room getRoomById(@PathVariable String id) {
        return roomRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("can't find room with id " + id));
    }
}
