package mysmarthome.services.resourceservices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import lombok.AllArgsConstructor;
import mysmarthome.models.Device;
import mysmarthome.models.DeviceDTO;
import mysmarthome.models.requestsfromclient.DeviceRequest;
import mysmarthome.repositories.DeviceRepository;
import mysmarthome.services.devicedtomapper.DeviceDTOMapper;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class DeviceResourceService {

    private final DeviceRepository deviceRepository;

    private final HttpRequestSender httpRequestSender;

    private final DeviceDTOMapper deviceDTOMapper;

    private final ObjectMapper objectMapper;

    public List<Device> getDeviceList() {
        return deviceRepository.findAll();
    }

    public Device getDeviceById(String id) {
        return deviceRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("can't find device with id " + id));
    }

    public DeviceDTO getDeviceDtoById(String id) {
        Device device = deviceRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("can't find device with id " + id));
        return deviceDTOMapper.apply(device);
    }

    public ResponseEntity<Void> turnOnOffDevice(DeviceRequest body) {
        return httpRequestSender.sendHttpRequest("/devices/actions", HttpMethod.POST, body);
    }

    public Device updateDevice(String id, JsonPatch patch) {
        Device originalDevice = deviceRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("can't find device with id " + id));
        Device devicePatched = applyPatchToDevice(patch, originalDevice);
        return deviceRepository.save(devicePatched);
    }

    private Device applyPatchToDevice(JsonPatch patch, Device targetDevice) {
        try {
            JsonNode patched = patch.apply(objectMapper.convertValue(targetDevice, JsonNode.class));
            return objectMapper.treeToValue(patched, Device.class);
        } catch (JsonPatchException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
