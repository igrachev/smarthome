package mysmarthome.services.resourceservices;

import lombok.RequiredArgsConstructor;
import mysmarthome.configuration.security.JwtUtils;
import mysmarthome.models.auth.AuthRequest;
import mysmarthome.repositories.AuthUserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthUserResourceService {

    private final AuthenticationManager authenticationManager;

    private final AuthUserRepository authUserRepository;

    private final JwtUtils jwtUtils;

    public String authenticateUser(AuthRequest authRequestBody) {
        String userName = authRequestBody.getUsername();
        String userPassword = authRequestBody.getPassword();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, userPassword));
        UserDetails user = authUserRepository.findByUsername(userName).orElseThrow(
                () -> new UsernameNotFoundException("can't find user with username " + userName));
        return jwtUtils.generateToken(user);
    }
}
