package mysmarthome.services.resourceservices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import lombok.AllArgsConstructor;
import mysmarthome.models.Group;
import mysmarthome.models.requestsfromclient.GroupRequest;
import mysmarthome.repositories.GroupRepository;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class GroupResourceService {

    private final HttpRequestSender httpRequestSender;

    private final ObjectMapper objectMapper;

    private final GroupRepository groupRepository;

    public List<Group> getGroupList() {
        return groupRepository.findAll();
    }

    public Group getGroupById(String id) {
        return groupRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("can't find group with id " + id));
    }

    public ResponseEntity<Void> turnOnOffGroup(String id, GroupRequest body) {
        return httpRequestSender.sendHttpRequest("/groups/" + id + "/actions", HttpMethod.POST, body);
    }

    public Group updateGroup(String id, JsonPatch patch) {
        Group originalGroup = groupRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("can't find group with id " + id));
        Group groupPatched = applyPatchToGroup(patch, originalGroup);
        groupRepository.save(groupPatched);
        return groupPatched;
    }

    private Group applyPatchToGroup(JsonPatch patch, Group targetGroup) {
        try {
            JsonNode patched = patch.apply(objectMapper.convertValue(targetGroup, JsonNode.class));
            return objectMapper.treeToValue(patched, Group.class);
        } catch (JsonPatchException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

