package mysmarthome.services.resourceservices;

import lombok.AllArgsConstructor;
import mysmarthome.models.Scenario;
import mysmarthome.repositories.ScenarioRepository;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ScenarioResourceService {

    private final ScenarioRepository scenarioRepository;

    private final HttpRequestSender httpRequestSender;

    public List<Scenario> getScenarioList() {
        return scenarioRepository.findAll();
    }

    public ResponseEntity<Void> turnOnOffScenario(String scenarioId) {
        return httpRequestSender.sendHttpRequest("/scenarios/" + scenarioId + "/actions", HttpMethod.POST);
    }
}
