package mysmarthome.services.resourceservices;

import lombok.AllArgsConstructor;
import mysmarthome.models.Household;
import mysmarthome.repositories.HouseholdRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class HouseholdResourceService {

    private final HouseholdRepository householdRepository;

    public List<Household> getHouseholdList() {
        return householdRepository.findAll();
    }
}
