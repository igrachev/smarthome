package mysmarthome.services.database;

import lombok.RequiredArgsConstructor;
import mysmarthome.models.BaseModel;
import mysmarthome.models.auth.AuthUser;
import mysmarthome.models.auth.Role;
import mysmarthome.models.responses.AllInfoResponse;
import mysmarthome.repositories.*;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DatabaseInitializer {

    @Value("${auth.username}")
    private String username;

    @Value("${auth.password}")
    private String password;

    private final DeviceRepository deviceRepository;

    private final RoomRepository roomRepository;

    private final GroupRepository groupRepository;

    private final ScenarioRepository scenarioRepository;

    private final HouseholdRepository householdRepository;

    private final HttpRequestSender httpRequestSender;

    private final AuthUserRepository authUserRepository;

    private final SmartScenarioRepository smartScenarioRepository;

    private final PasswordEncoder passwordEncoder;

    private final DatabaseSynchronizer databaseSynchronizer;

    @EventListener(ApplicationReadyEvent.class)
    public void handleApplicationReady() {
        AllInfoResponse allInfoResponse = httpRequestSender.sendHttpRequest("/user/info", HttpMethod.GET, AllInfoResponse.class);
        saveAll(deviceRepository, allInfoResponse.devices());
        saveAll(roomRepository, allInfoResponse.rooms());
        saveAll(groupRepository, allInfoResponse.groups());
        saveAll(scenarioRepository, allInfoResponse.scenarios());
        saveAll(householdRepository, allInfoResponse.households());
        authUserRepository.save(new AuthUser(1L, username, passwordEncoder.encode(password), Role.USER));
        databaseSynchronizer.updateDevices();
        databaseSynchronizer.updateGroups();
    }

    private <T extends BaseModel> void saveAll(JpaRepository<T, String> repository, List<T> objectsToSave) {
        repository.saveAll(objectsToSave);
    }
}
