package mysmarthome.services.database;

import lombok.RequiredArgsConstructor;
import mysmarthome.models.Device;
import mysmarthome.models.Group;
import mysmarthome.models.responses.GroupResponse;
import mysmarthome.repositories.DeviceRepository;
import mysmarthome.repositories.GroupRepository;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class DatabaseSynchronizer {

    private final DeviceRepository deviceRepository;

    private final GroupRepository groupRepository;

    private final HttpRequestSender httpRequestSender;

    @Scheduled(fixedDelay = 20000)
    public void run() {
        updateDevices();
        updateGroups();
    }

    public void updateDevices() {
        List<Device> devices = deviceRepository.findAll();
        devices.forEach(device -> {
            List<String> deviceSmartScenarios = device.getSmartScenarioNames();
            String household_id = device.getHouseholdId();
            boolean favouriteOld = device.isFavourite();
            Device response = httpRequestSender.sendHttpRequest("/devices/" + device.getId(), HttpMethod.GET, Device.class);
            Device deviceUpdated = Optional.ofNullable(response)
                    .orElseThrow(() -> new IllegalStateException("device with id " + device.getId() + "not found"));
            deviceRepository.save(deviceUpdated
                    .setSmartScenarioNames(deviceSmartScenarios)
                    .setDeviceFavourite(getDeviceFavourite(device.getId(), favouriteOld))
                    .setDeviceHousehold_id(household_id)
            );
        });
    }

    private boolean getDeviceFavourite(String id, boolean favouriteOld) {
        if (deviceRepository.findById(id).get().isFavourite() != favouriteOld) {
            return !favouriteOld;
        } else {
            return favouriteOld;
        }
    }

    public void updateGroups() {
        List<Group> groups = groupRepository.findAll();
        groups.forEach(group -> {
            String household_id = group.getHouseholdId();
            boolean favouriteOld = group.isFavourite();
            GroupResponse response = httpRequestSender.sendHttpRequest("/groups/" + group.getId(), HttpMethod.GET, GroupResponse.class);
            GroupResponse groupUpdatedResponse = Optional.ofNullable(response)
                    .orElseThrow(() -> new IllegalStateException("group with id " + group.getId() + "not found"));
            Group groupUpdated = Group.of(groupUpdatedResponse);
            groupRepository.save(groupUpdated
                    .setGroupFavourite(getGroupFavourite(group.getId(), favouriteOld))
                    .setGroupHouseholdId(household_id)
                    .setGroupDevices(groupUpdatedResponse.devices())
            );
        });
    }

    private boolean getGroupFavourite(String id, boolean favouriteOld) {
        if (groupRepository.findById(id).get().isFavourite() != favouriteOld) {
            return !favouriteOld;
        } else {
            return favouriteOld;
        }
    }
}
