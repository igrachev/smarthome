package mysmarthome.services.chat;

import lombok.extern.slf4j.Slf4j;
import mysmarthome.configuration.CurrentLocationProperties;
import mysmarthome.models.bodyforrequesttoyaapi.Device;
import mysmarthome.models.bodyforrequesttoyaapi.ManageCapabilityBody;
import mysmarthome.models.bodyforrequesttoyaapi.SmartScenarioResponse;
import mysmarthome.models.smartscenario.DeviceAction;
import mysmarthome.models.smartscenario.SmartScenario;
import mysmarthome.models.smartscenario.SmartScenarioAction;
import mysmarthome.repositories.DeviceRepository;
import mysmarthome.repositories.SmartScenarioRepository;
import mysmarthome.services.httprequestsender.HttpRequestSender;
import mysmarthome.services.jsonschemacreator.JsonSchemaCreator;
import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.client.advisor.QuestionAnswerAdvisor;
import org.springframework.ai.chat.client.advisor.api.CallAroundAdvisor;
import org.springframework.ai.openai.OpenAiChatOptions;
import org.springframework.ai.openai.api.ResponseFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SmartScenarioService {

    private final SmartScenarioRepository smartScenarioRepository;

    private final UserMessageService userMessageService;

    private final SystemMessageService systemMessageService;

    private final HttpRequestSender httpRequestSender;

    private final DeviceRepository deviceRepository;

    private final JsonSchemaCreator jsonSchemaCreator;

    private final ChatClient openAiChatClient;

    private final ChatClient configurableChatClient;

    private final CurrentLocationProperties currentLocationProperties;

    public SmartScenarioService(SmartScenarioRepository smartScenarioRepository,
                                UserMessageService userMessageService,
                                SystemMessageService systemMessageService,
                                HttpRequestSender httpRequestSender,
                                DeviceRepository deviceRepository,
                                JsonSchemaCreator jsonSchemaCreator,
                                CurrentLocationProperties currentLocationProperties,
                                QuestionAnswerAdvisor questionAnswerAdvisor,
                                CallAroundAdvisor loggingAdvisor,
                                @Qualifier("openAiChatClientBuilder") ChatClient.Builder openAiChatClientBuilder,
                                @Qualifier("configurableChatClientBuilder") ChatClient.Builder configurableChatClientBuilder) {
        this.smartScenarioRepository = smartScenarioRepository;
        this.userMessageService = userMessageService;
        this.systemMessageService = systemMessageService;
        this.httpRequestSender = httpRequestSender;
        this.deviceRepository = deviceRepository;
        this.jsonSchemaCreator = jsonSchemaCreator;
        this.currentLocationProperties = currentLocationProperties;
        this.openAiChatClient = openAiChatClientBuilder
                .defaultAdvisors(questionAnswerAdvisor, loggingAdvisor)
                .build();
        this.configurableChatClient = configurableChatClientBuilder
                .defaultAdvisors(loggingAdvisor)
                .build();
    }

    public List<SmartScenario> getAllByHouseholdId(String householdId) {
        return smartScenarioRepository.findAllByHouseholdId(householdId);
    }

    public SmartScenario createSmartScenario(String householdId, String roomId, String usersPrompt) {
        SmartScenarioResponse smartScenarioResponse = sendPromptToChat(usersPrompt, roomId);
        ManageCapabilityBody capabilityBody = smartScenarioResponse.getManageCapabilityBody();
        List<mysmarthome.models.Device> devices = deviceRepository.findAllById(getDeviceIdListManagedBySmartScenario(capabilityBody));
        SmartScenario smartScenario = new SmartScenario(getSmartScenarioName(usersPrompt), usersPrompt, roomId, householdId, true,
                List.of(getSmartScenarioAction(smartScenarioResponse, devices)));
        setCommentToNullAndSendPostToYaApi(capabilityBody);
        setSmartScenarioNameToDevices(smartScenario.getName(), devices);
        return smartScenarioRepository.save(smartScenario);
    }

    public SmartScenario activateSmartScenario(Long smartScenarioId) {
        SmartScenario smartScenario = smartScenarioRepository.findById(smartScenarioId).orElseThrow();
        smartScenario.setActive(true);
        return sendPromptToChatAndUpdateSmartScenario(smartScenario);
    }

    public SmartScenario startSmartScenario(Long smartScenarioId) {
        SmartScenario smartScenario = smartScenarioRepository.findById(smartScenarioId).orElseThrow();
        if (smartScenario.isActive()) {
            return sendPromptToChatAndUpdateSmartScenario(smartScenario);
        }
        return smartScenario;
    }

    private SmartScenario sendPromptToChatAndUpdateSmartScenario(SmartScenario smartScenario) {
        SmartScenarioResponse response = sendPromptToChat(smartScenario.getPrompt(), smartScenario.getRoomId());
        ManageCapabilityBody capabilityBody = response.getManageCapabilityBody();
        List<mysmarthome.models.Device> devices = deviceRepository.findAllById(getDeviceIdListManagedBySmartScenario(capabilityBody));
        setSmartScenarioNameToDevices(smartScenario.getName(), devices);
        smartScenario.setAction(getSmartScenarioAction(response, devices));
        setCommentToNullAndSendPostToYaApi(capabilityBody);
        return smartScenarioRepository.save(smartScenario);
    }

    private SmartScenarioAction getSmartScenarioAction(SmartScenarioResponse response, List<mysmarthome.models.Device> devices) {
        return new SmartScenarioAction(
                OffsetDateTime.now(Clock.systemUTC()).truncatedTo(ChronoUnit.SECONDS),
                String.join(" ", response.getReasoningSteps()),
                getDeviceActions(devices, response));
    }

    private List<DeviceAction> getDeviceActions(List<mysmarthome.models.Device> devices, SmartScenarioResponse response) {
        List<DeviceAction> deviceActions = new ArrayList<>();
        devices.forEach(device -> deviceActions.addAll(getDeviceAction(device, response.getManageCapabilityBody().getDevices())));
        return deviceActions;
    }

    private List<DeviceAction> getDeviceAction(mysmarthome.models.Device device, List<Device> devicesWithActions) {
        return devicesWithActions.stream()
                .filter(deviceWithAction -> deviceWithAction.getId().equals(device.getId()))
                .flatMap(deviceWithAction -> deviceWithAction.getActions().stream())
                .map(action -> new DeviceAction(device.getName(),
                        action.getState().getInstance(),
                        action.getState().getValue().toString(),
                        action.getComment()))
                .collect(Collectors.toList());
    }

    public SmartScenario deactivateSmartScenario(Long smartScenarioId) {
        SmartScenario smartScenario = smartScenarioRepository.findById(smartScenarioId).orElseThrow();
        removeSmartScenarioNameFromDevices(smartScenario);
        smartScenario.setActive(false);
        return smartScenarioRepository.save(smartScenario);
    }

    private void removeSmartScenarioNameFromDevices(SmartScenario smartScenario) {
        HashSet<String> deviceNames = getUniqueDeviceNames(smartScenario.getActions());
        deviceNames.stream()
                .map(deviceRepository::findByName)
                .forEach(device -> device.removeSmartScenarioName(smartScenario.getName()));
    }

    private HashSet<String> getUniqueDeviceNames(List<SmartScenarioAction> actions) {
        return actions.stream()
                .flatMap(action -> action.getDeviceActions().stream())
                .map(DeviceAction::getDevice)
                .collect(Collectors.toCollection(HashSet::new));
    }

    public void deleteSmartScenario(Long smartScenarioId) {
        SmartScenario smartScenario = smartScenarioRepository.findById(smartScenarioId).orElseThrow();
        removeSmartScenarioNameFromDevices(smartScenario);
        smartScenarioRepository.deleteById(smartScenarioId);
    }

    private void setSmartScenarioNameToDevices(String smartScenarioName, List<mysmarthome.models.Device> devices) {
        devices.forEach(device -> device.setSmartScenarioName(smartScenarioName));
        deviceRepository.saveAll(devices);
    }

    private SmartScenarioResponse sendPromptToChat(String usersPrompt, String roomId) {
        return openAiChatClient.prompt()
                .system(systemMessageService.getSmartScenarioMessage())
                .user(u -> u.text("""
                                Current date and time: {currentDateAndTime}
                                House located in city: {currentCity}
                                RoomId: {roomId}
                                Condition in the room: {roomCondition}
                                User's requirement: {userRequirement}
                                Devices with capabilities to control: {devices}
                                
                                Reread user's requirement again: {userRequirement}
                                """)
                        .params(Map.of("currentDateAndTime", userMessageService.getDateAndTime(),
                                "currentCity", currentLocationProperties.city(),
                                "roomId", roomId,
                                "roomCondition", userMessageService.getRoomCondition(roomId),
                                "userRequirement", usersPrompt,
                                "devices", userMessageService.getDevicesWithOnlineStateAndCapabilities(roomId))))
                .options(OpenAiChatOptions.builder()
                        .withResponseFormat(new ResponseFormat(ResponseFormat.Type.JSON_SCHEMA,
                                jsonSchemaCreator.getSmartScenarioJsonSchema()))
                        .withFunctions(Set.of("currentWeatherFunction", "motionDetectorInLastGivenMinutesFunction"))
                        .build())
                .call()
                .entity(SmartScenarioResponse.class);
    }

    private List<String> getDeviceIdListManagedBySmartScenario(ManageCapabilityBody capabilityBody) {
        return capabilityBody.getDevices().stream()
                .map(Device::getId)
                .collect(Collectors.toList());
    }

    private void setCommentToNullAndSendPostToYaApi(ManageCapabilityBody capabilityBody) {
        capabilityBody.getDevices().forEach(device -> device.getActions().forEach(action -> action.setComment(null)));
        if (capabilityBody.getDevices().isEmpty()) {
            log.info("No request to ya API needed since there are no devices to control");
            return;
        }
        log.info("Sending SmartScenario request to ya API");
        httpRequestSender.sendHttpRequest("/devices/actions", HttpMethod.POST, capabilityBody);
    }

    private String getSmartScenarioName(String prompt) {
        return configurableChatClient.prompt()
                .system(systemMessageService.getSmartScenarioName())
                .user(prompt)
                .call()
                .content();
    }
}
