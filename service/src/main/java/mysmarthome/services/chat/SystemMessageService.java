package mysmarthome.services.chat;

import lombok.RequiredArgsConstructor;
import mysmarthome.repositories.DeviceRepository;
import mysmarthome.repositories.GroupRepository;
import mysmarthome.models.Device;
import mysmarthome.models.Group;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SystemMessageService {

    private static final String NON_INCLUSION_MESSAGE = """
            - ALWAYS compare current capability state of devices with the desired one:
              if statuses are the same then do not include this action in the answer EVEN IF USER EXPLICITLY ASKED IT,
              for example:
              user requires to turn on some device, but its current  capability state is already on:
              "capabilities": [{"type": "devices.capabilities.on_off","state": {"instance": "on","value": "true”}}],
              then do not include this device in the answer
            - If all devices are in the desired state, return an empty list of devices.
            """;

    private static final String GENERAL_INSTRUCTION = """
            You will be provided with information about the current conditions of devices in the household delimited with triple quotes.
            You will be also provided with question starting with the word 'Question:'.
            Use the following instruction to respond to user inputs:
            """;

    private static final String GENERAL_INSTRUCTION_DEVICE = """
            Take into consideration other devices in the room if it's relevant for analyses.
            Also take into consideration if there are any scenarios related to this device.
            Answer in 4 sentences.
            """;

    private final DeviceRepository deviceRepository;

    private final GroupRepository groupRepository;

    String getChatMessage(String message) {
        return String.format("""
                You are a helpful home assistant acting like Mister Spock - Star Trek movie character.
                You should answer only questions related to the household and it's devices
                or current weather conditions with relation to the household.
                If you are asked anything which is not related to current home just answer like Mister Spock would do it
                like that it is not logical to ask anything that is not related to this home and include a quote from Mister Spoke.
                %s%s""", GENERAL_INSTRUCTION, getChatMessageInstructions(message));
    }

    private String getChatMessageInstructions(String message) {
        if (message.equals("Provide general info")) {
            return """
                    Analyze the household and provide general information about it regarding the following:
                    comfort based on temperature, humidity, pressure;
                    lighting and illumination data from sensors in the household with respect to
                    current time and times of day and light devices switched on or off;
                    battery change if needed;
                    active scenarios.
                    Answer in 5 sentences.
                    """;
        }
        return "Be descriptive, provide detailed analyses and answer in 2 sentences.\n";
    }

    String getDeviceAnalysesMessage(String deviceId) {
        Device device = deviceRepository.findById(deviceId).orElseThrow();
        return getAnalysesMessage(device.getType());
    }

    String getGroupAnalysesMessage(String groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow();
        return getAnalysesMessage(group.getType());
    }

    private String getAnalysesMessage(String type) {
        return switch (type) {
            case "devices.types.light" -> String.format("""
                    %s As a background for your analyses use info regarding the actual lightning
                    in the room or apartment from any sensor with illumination info if present and
                    from the current time and times of day.
                    If you have info like brightness and temperature from this device,
                    analyze what is it comfortable for (like reading, daily activities and computer mode) and
                    give recommendations for adjusting it if needed.
                    If you don't have this info, just analyze based on provided info.%s
                    """, GENERAL_INSTRUCTION, GENERAL_INSTRUCTION_DEVICE);
            case "devices.types.humidifier" -> String.format("""
                    %s Get the actual temperature and humidity in the room or apartment from any sensor device if present.
                    Compare it to the desired temperature and humidity level of the humidifier and
                    analyze whether humidifier needs to be switched on or off and whether its temperature and
                    humidity settings need to be adjusted.%s
                    """, GENERAL_INSTRUCTION, GENERAL_INSTRUCTION_DEVICE);
            case "devices.types.sensor" -> String.format("""
                    %s if it contains any info related to temperature, humidity, pressure or illumination then
                    provide detailed analyses of this data with respect to the generally accepted standards of indoor comfort.
                    If it contains any info related to battery level then provide recommendations for battery change if needed.
                    If you don't have info related to temperature, humidity, pressure, illumination or battery level,
                    just analyze this device based on provided info and doesn't reply that you don't have this info.%s
                    """, GENERAL_INSTRUCTION, GENERAL_INSTRUCTION_DEVICE);
            case "devices.types.vacuum_cleaner" -> String.format("""
                    %s Take into consideration the size of the house based on the number of rooms and provide recommendations
                    how often this robot vacuum cleaner should be used to keep the house clean.%s
                    """, GENERAL_INSTRUCTION, GENERAL_INSTRUCTION_DEVICE);
            default -> String.format("""
                    %s%s""", GENERAL_INSTRUCTION, GENERAL_INSTRUCTION_DEVICE);
        };
    }

    String getSmartScenarioMessage() {
        return String.format("""
                ----------------------------------------------
                
                You are a home assistant and you should control smart devices in the household based on user's requirement.
                
                ----------------------------------------------
                
                 You will be provided with following information:
                
                - Current date and time
                - Current house location (city)
                - RoomId
                - Condition in the room: temperature, humidity, pressure, illumination, window/door state
                - User's requirement
                - Devices info: state and capabilities to control
                
                ______________________________________________
                
                Follow these instructions to provide the response:
                %s
                ----------------------------------------------
                
                Follow these rules to call the functions:
                
                - currentWeatherFunction: call it only if you need some info about the weather and outdoor conditions
                - motionDetectorInLastGivenMinutesFunction: call it only if you need to check if motion was detected in the room
                """, NON_INCLUSION_MESSAGE);
    }

    String getSmartScenarioName() {
        return """
                You will be provided with user's requirement regarding control of smart devices in the household.
                Focusing on activity and needed condition for it summarize it in one sentence
                consisted of 6 - 7 words and max 44 characters""";
    }
}
