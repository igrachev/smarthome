package mysmarthome.services.chat;

import lombok.extern.slf4j.Slf4j;
import mysmarthome.models.chat.AssistantResponse;
import mysmarthome.services.jsonschemacreator.JsonSchemaCreator;
import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.client.advisor.api.CallAroundAdvisor;
import org.springframework.ai.openai.OpenAiChatOptions;
import org.springframework.ai.openai.api.ResponseFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ChatService {

    private final ChatClient chatClient;

    private final UserMessageService userMessageService;

    private final SystemMessageService systemMessageService;

    private final JsonSchemaCreator jsonSchemaCreator;

    public ChatService(@Qualifier("openAiChatClientBuilder") ChatClient.Builder chatClientBuilder,
                       UserMessageService userMessageService,
                       SystemMessageService systemMessageService,
                       JsonSchemaCreator jsonSchemaCreator,
                       CallAroundAdvisor loggingAdvisor,
                       CallAroundAdvisor reReadingAdvisor) {
        this.chatClient = chatClientBuilder
                .defaultAdvisors(loggingAdvisor, reReadingAdvisor)
                .build();
        this.userMessageService = userMessageService;
        this.systemMessageService = systemMessageService;
        this.jsonSchemaCreator = jsonSchemaCreator;
    }

    public AssistantResponse sendRequest(String householdId, String usersQuestion) {
        return getAssistantResponse(systemMessageService.getChatMessage(usersQuestion),
                userMessageService.getChatMessage(householdId, usersQuestion));
    }

    public AssistantResponse analyseDevice(String householdId, String deviceId) {
        return getAssistantResponse(systemMessageService.getDeviceAnalysesMessage(deviceId),
                userMessageService.getDeviceAnalysesMessage(householdId, deviceId));
    }

    public AssistantResponse analyseGroup(String householdId, String groupId) {
        return getAssistantResponse(systemMessageService.getGroupAnalysesMessage(groupId),
                userMessageService.getGroupAnalysesMessage(householdId, groupId));
    }

    private AssistantResponse getAssistantResponse(String createdSystemMessage, String createdUserMessage) {
        return chatClient.prompt()
                .system(createdSystemMessage)
                .user(createdUserMessage)
                .options(OpenAiChatOptions.builder()
                        .withResponseFormat(new ResponseFormat(ResponseFormat.Type.JSON_SCHEMA,
                                jsonSchemaCreator.getAssistantResponseJsonSchema()))
                        .withFunction("currentWeatherFunction")
                        .build())
                .call()
                .entity(AssistantResponse.class);
    }
}
