package mysmarthome.services.chat.functioncallingservices;

import lombok.extern.slf4j.Slf4j;
import mysmarthome.configuration.WeatherConfigProperties;
import mysmarthome.models.weather.Request;
import mysmarthome.models.weather.Response;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.util.function.Function;

@Slf4j
@Service
public class WeatherService implements Function<Request, Response> {

    private final RestClient restClient;

    private final WeatherConfigProperties weatherConfigProperties;

    public WeatherService(WeatherConfigProperties weatherConfigProperties) {
        this.weatherConfigProperties = weatherConfigProperties;
        this.restClient = RestClient.create(weatherConfigProperties.apiUrl());
    }

    @Override
    public Response apply(Request request) {
        Response response = restClient.get()
                .uri("/current.json?key={key}&q={city}", weatherConfigProperties.apiKey(), request.city())
                .retrieve()
                .body(Response.class);
        log.info("Function was called: currentWeatherFunction, result: {}", getResult(request, response));
        return response;
    }

    private String getResult(Request request, Response response) {
        return "Current weather in " + request.city() + " is " + response.current().condition().text() +
                " with temperature " + response.current().temp_c() + "°C, humidity " + response.current().humidity() + "% and wind speed " +
                response.current().wind_kph() + " km/h";
    }
}
