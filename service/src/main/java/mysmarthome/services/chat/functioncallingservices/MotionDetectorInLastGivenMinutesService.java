package mysmarthome.services.chat.functioncallingservices;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mysmarthome.configuration.CurrentLocationProperties;
import mysmarthome.models.Device;
import mysmarthome.models.Property;
import mysmarthome.repositories.DeviceRepository;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@RequiredArgsConstructor
@Slf4j
@Service
public class MotionDetectorInLastGivenMinutesService implements Function<MotionDetectorInLastGivenMinutesService.Request, MotionDetectorInLastGivenMinutesService.Response> {

    private final DeviceRepository deviceRepository;

    private final CurrentLocationProperties currentLocationProperties;

    @Override
    public Response apply(Request request) {
        OffsetDateTime currentTime = OffsetDateTime.parse(request.currentTime);
        int minutes = Integer.parseInt(request.minutes);

        List<Device> devices = deviceRepository.findAllByRoom(request.roomId);
        Optional<Property> propertyOptional = devices.stream()
                .flatMap(device -> device.getProperties().stream())
                .filter(property -> property.getState() != null && property.getState().getInstance() != null &&
                        property.getState().getInstance().equals("motion"))
                .findFirst();

        if(propertyOptional.isEmpty()) {
            String result = "No data available for motion detection";
            log.info("Function was called: motionDetectorInLastGivenMinutesFunction, result: {}", result);
            return new Response(result);
        } else {
            OffsetDateTime lastMotionDetectedTime = propertyOptional.get().getLastUpdated()
                    .atZoneSameInstant(ZoneId.of("Europe/" + currentLocationProperties.city())).toOffsetDateTime().withNano(0);
            boolean motionNotDetected = currentTime.isAfter(lastMotionDetectedTime.plusMinutes(minutes));
            String result = motionNotDetected ? "No motion detected in the last " + minutes + " minutes" :
                    "Motion was detected in the last " + minutes + " minutes";
            log.info("Function was called: motionDetectorInLastGivenMinutesFunction, result: {}", result);
            return new Response(result);
        }
    }

    public record Request(String roomId, String currentTime, String minutes) {
    }

    public record Response(String motionDetectionInLastGivenMinutes) {
    }
}


