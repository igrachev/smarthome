package mysmarthome.services.chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import mysmarthome.configuration.CurrentLocationProperties;
import mysmarthome.models.*;
import mysmarthome.models.chat.DeviceDTO;
import mysmarthome.repositories.*;
import mysmarthome.services.devicedtomapper.DeviceDTOChatMapper;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class UserMessageService {

    private final DeviceRepository deviceRepository;

    private final RoomRepository roomRepository;

    private final GroupRepository groupRepository;

    private final HouseholdRepository householdRepository;

    private final ScenarioRepository scenarioRepository;

    private final DeviceDTOChatMapper deviceDTOChatMapper;

    private final CurrentLocationProperties currentLocationProperties;

    private final ObjectMapper objectMapper;

    String getChatMessage(String householdId, String usersQuestion) {
        String generalInfo = getGeneralInfoAboutHome(householdId);
        return String.format("%s. Question: %s", generalInfo, usersQuestion);
    }

    String getDeviceAnalysesMessage(String householdId, String deviceId) {
        Device device = deviceRepository.findById(deviceId).orElseThrow();
        Room room = roomRepository.findById(device.getRoom()).orElseThrow();
        String generalInfo = getGeneralInfoAboutHome(householdId);
        return String.format("%s. Question: Analyze device %s in the room %s", generalInfo, device.getName(), room.getName());
    }

    String getGroupAnalysesMessage(String householdId, String groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow();
        String generalInfo = getGeneralInfoAboutHome(householdId);
        return String.format("%s. Question: Analyze group %s", generalInfo, group.getName());
    }

    String getDevicesWithOnlineStateAndCapabilities(String roomId) {
        List<Device> devices = deviceRepository.findAllByRoom(roomId);
        List<DeviceDTO> devicesDTOList = devices.stream()
                .filter(device -> !"offline".equals(device.getState()) && device.getCapabilities() != null && !device.getCapabilities().isEmpty())
                .map(deviceDTOChatMapper)
                .toList();
        String deviceDTOJson;
        try {
            deviceDTOJson = objectMapper.writeValueAsString(devicesDTOList);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return deviceDTOJson;
    }

    String getRoomCondition(String roomId) {
        List<Device> devices = deviceRepository.findAllByRoom(roomId);
        return getRoomConditionByInstances(devices, List.of("temperature", "humidity", "pressure", "illumination"));
    }

    private String getRoomConditionByInstances(List<Device> devices, List<String> instances) {
        StringBuilder result = new StringBuilder();
        devices.stream()
                .flatMap(device -> device.getProperties().stream())
                .filter(property -> property.getState() != null && property.getState().getInstance() != null &&
                        instances.contains(property.getState().getInstance()))
                .forEach(property -> result.append(property.getState().getInstance()).append(": ")
                        .append(property.getState().getValue()).append(" ")
                        .append(truncateUpToLastWordAfterDot(property.getParameters().getUnit())).append(", "));

        String windowStateInfo = getWindowStateInfo(devices);
        if (!windowStateInfo.isEmpty()) {
            result.append(windowStateInfo);
        }

        return result.toString();
    }

    private String getWindowStateInfo(List<Device> devices) {
        return devices.stream()
                .filter(this::hasOpenState)
                .findFirst()
                .map(device -> device.getName() + ": " + getOpenStateValue(device))
                .orElse("");
    }

    private boolean hasOpenState(Device device) {
        return device.getProperties().stream()
                .anyMatch(this::isOpenState);
    }

    private String getOpenStateValue(Device device) {
        return device.getProperties().stream()
                .filter(this::isOpenState)
                .map(property -> property.getState().getValue())
                .findFirst()
                .orElse("");
    }

    private boolean isOpenState(Property property) {
        return property.getState() != null && "open".equals(property.getState().getInstance());
    }

    OffsetDateTime getDateAndTime() {
        return OffsetDateTime.now().atZoneSameInstant(ZoneId.of("Europe/" + currentLocationProperties.city())).toOffsetDateTime().withNano(0);
    }

    String getGeneralInfoAboutHome(String householdId) {
        Household household = householdRepository.findById(householdId).orElseThrow();
        List<Device> deviceList = deviceRepository.findAllByHouseholdId(household.getId());
        List<Room> roomList = roomRepository.findAllByHouseholdId(household.getId());
        List<Group> groupList = groupRepository.findAllByHouseholdId(household.getId());
        List<Scenario> scenarioList = scenarioRepository.findAll();
        StringBuilder message = new StringBuilder();
        message.append("\"\"\"Current date and time is: ").append(getDateAndTime()).append(". ");
        message.append(" Household \"").append(household.getName()).append(" located in city ").append(currentLocationProperties.city())
                .append(" consists of following rooms with devices: ");
        createRoomToDevisesMapAndGetInfo(message, roomList, deviceList);
        getGroupInfo(message, groupList, deviceList);
        getScenarioInfo(message, scenarioList);
        message.append("\"\"\"");
        return message.toString();
    }

    private void createRoomToDevisesMapAndGetInfo(StringBuilder message, List<Room> roomList, List<Device> deviceList) {
        Map<Room, List<Device>> roomToDevicesMap = new HashMap<>();
        roomList.forEach(room -> roomToDevicesMap.put(room, getDeviceList(deviceList, room.getDevices())));
        getRoomToDevicesInfo(message, roomToDevicesMap);
    }

    private void getRoomToDevicesInfo(StringBuilder message, Map<Room, List<Device>> roomToDevicesMap) {
        roomToDevicesMap.forEach((room, devices) ->
                message.append("room \"").append(room.getName()).append("\" consists of following devices: ")
                        .append(getDeviceListInfo(devices)));
    }

    private String getDeviceListInfo(List<Device> deviceList) {
        StringBuilder result = new StringBuilder();
        deviceList.forEach(device -> result.append(getDeviceInfo(device)).append(", "));
        return result.toString();
    }

    private String getDeviceInfo(Device device) {
        return String.format("\"%s\" (type: %s, favourite: %b, state: %s%s%s%s)", device.getName(),
                truncateUpToLastWordAfterDot(device.getType()), device.isFavourite(), device.getState(),
                getAliases(device.getAliases()),
                getCharacteristicInfo(new ArrayList<>(device.getProperties())),
                getCharacteristicInfo(new ArrayList<>(device.getCapabilities())));
    }

    private void getGroupInfo(StringBuilder message, List<Group> groupList, List<Device> deviceList) {
        if (!groupList.isEmpty()) {
            Map<Group, List<Device>> groupToDevicesMap = new HashMap<>();
            groupList.forEach(group -> groupToDevicesMap.put(group, getDeviceList(deviceList, group.getDevices())));
            message.append(groupList.size() == 1 ? "There is " : "There are ").append(groupList.size())
                    .append(groupList.size() == 1 ? " group" : " groups").append(" of devices in the household: ");
            groupToDevicesMap.forEach((group, devices) -> message.append(getGroupInfo(group))
                    .append(" consists of ").append(devices.size()).append(" devices: ").append(getDeviceNameList(devices))
            );
        }
    }

    private List<Device> getDeviceList(List<Device> deviceList, List<String> deviceIdList) {
        return deviceList.stream()
                .filter(device -> deviceIdList.contains(device.getId()))
                .collect(Collectors.toList());
    }

    private void getScenarioInfo(StringBuilder message, List<Scenario> scenarioList) {
        message.append("There are scenarios: ");
        scenarioList.forEach(scenario -> message.append("\"").append(scenario.getName()).append("\" (online: ")
                .append(scenario.getIs_active()).append("), "));
    }

    private String getGroupInfo(Group group) {
        return String.format("\"%s\" (type: %s, favourite: %b, state: %s%s%s)", group.getName(),
                truncateUpToLastWordAfterDot(group.getType()), group.isFavourite(), getGroupState(group.getState()),
                getAliases(group.getAliases()),
                getCharacteristicInfo(new ArrayList<>(group.getCapabilities())));
    }

    private String getGroupState(String state) {
        return "split".equals(state) ? "online" : state;
    }

    private String getDeviceNameList(List<Device> deviceList) {
        StringBuilder result = new StringBuilder();
        deviceList.forEach(device -> result.append(device.getName()).append(", "));
        return result.toString();
    }

    private String getAliases(List<String> aliases) {
        return aliases.isEmpty() ? "" : ", aliases: " + String.join(", ", aliases);
    }

    private String getCharacteristicInfo(List<Characteristic> characteristicList) {
        StringBuilder characteristicInfo = new StringBuilder();
        characteristicList.stream()
                .filter(characteristic -> characteristic.getState() != null)
                .forEach(characteristic -> {
                    characteristicInfo.append(", ")
                            .append(characteristic.getState().getInstance()).append(": ")
                            .append(characteristic.getState().getValue());
                    if (characteristic.getParameters().getUnit() != null) {
                        characteristicInfo.append(" ").append(truncateUpToLastWordAfterDot(characteristic.getParameters().getUnit()));
                    }
                    if (characteristic.getState().getInstance().equals("\"motion\"") && !characteristic.getLastUpdated().toString().startsWith("1970")) {
                        characteristicInfo.append(" (last detected at: ").append(getLastUpdated(characteristic.getLastUpdated())).append(")");
                    }
                });
        return characteristicInfo.toString();
    }

    private OffsetDateTime getLastUpdated(OffsetDateTime offsetDateTime) {
        return offsetDateTime.atZoneSameInstant(ZoneId.of("Europe/" + currentLocationProperties.city())).toOffsetDateTime().withNano(0);
    }

    private String truncateUpToLastWordAfterDot(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }
        String[] segments = input.split("\\.");
        String lastSegment = segments[segments.length - 1];
        return lastSegment.equals("open") ? segments[segments.length - 2] : lastSegment;
    }
}
