package mysmarthome.services.devicedtomapper;

import mysmarthome.models.Device;
import mysmarthome.models.DeviceDTO;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class DeviceDTOMapper implements Function<Device, DeviceDTO> {

    @Override
    public DeviceDTO apply(Device device) {
        return new DeviceDTO(device.getId(), device.getName(), device.getState(), device.getCapabilities());
    }
}
