package mysmarthome.services.devicedtomapper;

import mysmarthome.models.Capability;
import mysmarthome.models.Device;
import mysmarthome.models.chat.DeviceDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
public class DeviceDTOChatMapper implements Function<Device, DeviceDTO> {

    @Override
    public DeviceDTO apply(Device device) {

        return new DeviceDTO(device.getId(), device.getName(), device.getType(), returnNullIfEmptyOrNull(device.getAliases()),
                updateCharacteristic(device.getCapabilities()));
    }

    private List<String> returnNullIfEmptyOrNull(List<String> aliases) {
        return aliases == null || aliases.isEmpty() ? null : aliases;
    }

    private List<Capability> updateCharacteristic(List<Capability> capabilities) {
        capabilities.forEach(capability -> {
            capability.setLastUpdated(null);
            if (capability.getParameters() != null && capability.getParameters().getSplit() != null) {
                capability.setParameters(null);
            }
        });
        return capabilities;
    }
}
