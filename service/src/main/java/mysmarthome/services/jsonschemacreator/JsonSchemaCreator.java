package mysmarthome.services.jsonschemacreator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@NoArgsConstructor
@AllArgsConstructor
@Data
public class JsonSchemaCreator {

    @Value("classpath:/jsonschema/smart-scenario-schema.json")
    private Resource smartScenarioJsonSchema;

    @Value("classpath:/jsonschema/assistant-response-schema.json")
    private Resource assistantResponseSchema;

    public String getAssistantResponseJsonSchema() {
        return getJsonSchema(assistantResponseSchema);
    }

    public String getSmartScenarioJsonSchema() {
        return getJsonSchema(smartScenarioJsonSchema);
    }

    private String getJsonSchema(Resource resource) {
        try (var inputStream = resource.getInputStream()) {
            return new String(inputStream.readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
