package mysmarthome.services.httprequestsender;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.spring.LogbookClientHttpRequestInterceptor;

@Service
public class HttpRequestSender {

    private final RestClient restClient;


    public HttpRequestSender(RestClient.Builder restClientBuilder,
                             @Value("${ya.base-url}") String yaBaseUrl,
                             @Value("${ya.token}") String accessToken,
                             Logbook logbook) {
        this.restClient = restClientBuilder
//                .requestInterceptor(new LogbookClientHttpRequestInterceptor(logbook))
                .baseUrl(yaBaseUrl)
                .defaultHeader("Authorization", accessToken)
                .build();
    }

    public <T> T sendHttpRequest(String uri, HttpMethod method, Class<T> responseType) {
        return restClient.method(method)
                .uri(uri)
                .retrieve()
                .body(responseType);
    }

    public ResponseEntity<Void> sendHttpRequest(String uri, HttpMethod method) {
        return restClient.method(method)
                .uri(uri)
                .retrieve()
                .toBodilessEntity();
    }

    public <T> ResponseEntity<Void> sendHttpRequest(String uri, HttpMethod method, T body) {
        return restClient.method(method)
                .uri(uri)
                .body(body)
                .retrieve()
                .toBodilessEntity();
    }
}
