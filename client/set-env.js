const fs = require('fs');
const path = require('path');

// Specify the environment file path
const targetPathNonProd = path.join(__dirname, './src/environments/environment.ts');
const targetPathProd = path.join(__dirname, './src/environments/environment.prod.ts')

// Read environment variables
const authUsername = process.env.AUTH_USER_NAME || 'user';
const authPassword = process.env.AUTH_PASSWORD || 'pw';

// Prepare content to write to the environment file
const envConfigFileNonProd = `export const environment = {
  production: false,
  apiUrl: 'http://localhost:8080',
  authUsername: '${authUsername}',
  authPassword: '${authPassword}'
};
`;

const envConfigFileProd = `export const environment = {
  production: true,
  apiUrl: '',
  authUsername: '${authUsername}',
  authPassword: '${authPassword}'
};
`;

// Write to the environment file
fs.writeFileSync(targetPathNonProd, envConfigFileNonProd, 'utf8');
fs.writeFileSync(targetPathProd, envConfigFileProd, 'utf8');

console.log(`Environment file generated at ${targetPathNonProd} and ${targetPathProd}`);
