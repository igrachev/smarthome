import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {Household} from "../households/household";

@Component({
  selector: 'app-home',
  template: `
    <div id="wrapper1">
      <header>
        <app-households
          (select)="navigateToHousehold($event)"
          (error)="setError($event)"
        ></app-households>
        <app-smart-scenario-header
          [householdId]="householdId"
        ></app-smart-scenario-header>
      </header>
      <div *ngIf="!error">
        <router-outlet></router-outlet>
      </div>
      <div *ngIf="error" id="error-content">
        <h2 id="error-title">Error occured</h2>
        <p id="error-message">{{ error }}</p>
        <button id="error-button" (click)="reload()">Reload page</button>
      </div>
      <footer>
        <div id="more-info">
          <a id="more-info-link" href="https://yandex.ru/dev/dialogs/smart-home/doc/concepts/platform-user-info.html"
             target="_blank">More Information</a>
        </div>
      </footer>
    </div>
  `,
  styleUrls: ["./home.component.css"]
})
export class HomeComponent {

  error?: string;
  householdId?: string;
  householdName?: string;

  constructor(private router: Router) {
  }

  navigateToHousehold(household: Household) {
    this.error = undefined;
    this.householdId = household.id;
    this.householdName = household.name;
    this.router.navigate(['main', this.householdId, this.householdName]);
  }

  setError(error: string) {
    this.error = error;
  }

  reload() {
    window.location.reload();
  }
}
