import {Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import {SmartHomeService} from "../services/smart-home.service";
import {Scenario} from "./scenario";

@Component({
  selector: 'app-scenario',
  template: `
    <div id="wrapper" *ngIf="mainIsLoading === false">
      <h3>Scenarios</h3>
      <ul>
        <li *ngFor="let scenario of scenarios | filterScenario:limit">
          <div id="scenarios">
            <div (mouseenter)="hoverIn(scenario)" (mouseleave)="hoverOut(scenario)"
                 [ngStyle]="{'color': (scenario.is_active === 'true') ? 'lightgrey':'darkgrey'}">
              {{ scenario.name | shortenScenarioName:scenario.is_active }}
              <span *ngIf="scenario.is_active === 'false'" id="inactive">| Inactive</span>
            </div>
            <div id="show-fullname" *ngIf="scenario.hover">
              {{ scenario.name }}
            </div>
            <div *ngIf="scenario.is_active === 'true'" id="active-scenario" (click)="activateScenario(scenario.id)">▶️
            </div>
          </div>
        </li>
      </ul>
      <div id="all-scenarios">
        <a id="all-scenarios-1" [routerLink]="['/scenarios', this.householdId, this.householdName]">All scenarios</a>
      </div>
    </div>
  `,
  styleUrls: ["./scenario.component.css"]
})

export class ScenarioComponent implements OnInit, OnDestroy {
  @Output() error = new EventEmitter<string>();
  @Output() scenariosAreLoading = new EventEmitter<boolean>();
  @Input() householdName?: string;
  @Input() householdId?: string;
  @Input() mainIsLoading?: boolean;
  limit = 10;
  scenarios: Scenario[] = [];

  constructor(private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.adjustLimitBasedOnWindowSize();
    window.addEventListener('resize', this.adjustLimitBasedOnWindowSize.bind(this));
    this.scenariosAreLoading.emit(true);
    this.smartHomeService.retrieveAllScenarios()
      .then(scenarios => this.scenarios = scenarios)
      .then(scenarios => scenarios.sort(
        (s1, s2) => s1.name.localeCompare(s2.name)
      ))
      .then(() => this.scenariosAreLoading.emit(false))
      .catch(error => this.error.emit(error.message));
  }

  ngOnDestroy(): void {
    window.removeEventListener('resize', this.adjustLimitBasedOnWindowSize.bind(this));
  }

  activateScenario(id: string) {
    this.smartHomeService.activateScenario(id);
  }

  hoverIn(scenario: Scenario) {
    if (scenario.is_active === 'false' && scenario.name.length > 16) {
      scenario.hover = true;
    } else if (scenario.is_active === 'true' && scenario.name.length > 20) {
      scenario.hover = true;
    }
  }

  hoverOut(scenario: Scenario) {
    scenario.hover = false;
  }

  private adjustLimitBasedOnWindowSize() {
    let height = 440;
    for (let i = 1; i < 13; i++) {
      if (window.innerHeight < height) {
        this.limit = i;
        break;
      } else {
        height = height + 48;
      }
    }
  }
}
