import {Component, OnInit} from '@angular/core';
import {SmartHomeService} from "../../services/smart-home.service";
import {Scenario} from "../scenario";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-allscenarios',
  template: `
    <div id="wrapper1">
      <div class="displayed"></div>
      <div id="wrapper2" *ngIf="!isLoading && !error">
        <p id="scenarioTitle">Scenarios</p>
        <label id="input-name">
          <input type="text" [(ngModel)]="filerScenario" placeholder="Search scenario">
        </label>
        <ul>
          <li *ngFor="let scenario of scenarios | filterScenarioName:filerScenario">
            <div id="scenario-block">
              <div id="scenario-name" [ngStyle]="{'color': (scenario.is_active === 'true') ? 'lightgrey':'dimgrey'}">
                {{ scenario.name }}
                <span *ngIf="scenario.is_active === 'false'" id="inactive">| Inactive</span>
              </div>
              <div *ngIf="scenario.is_active === 'true'" id="scenario-start-on" (click)="activateScenario(scenario.id)">
                ▶️
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div *ngIf="isLoading && !error" class="wifi-outside">
        <div class="wifi-inside">
          <div class="wifi"></div>
        </div>
      </div>
      <div *ngIf="error" id="error-content">
        <h2 id="error-title">Error occurred</h2>
        <p id="error-message">{{ error }}</p>
        <button id="error-button" (click)="onHandleError()">Back to start page</button>
      </div>
      <div class="displayed"></div>
    </div>
  `,
  styleUrls: ["./allscenarios.component.css"]
})
export class AllscenariosComponent implements OnInit {
  householdName?: string;
  householdId?: string;
  scenarios: Scenario[] = [];
  isLoading = false;
  error?: string;
  filerScenario = '';

  constructor(private route: Router, private activatedRoute: ActivatedRoute, private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.activatedRoute.params.subscribe(
      params => {
        this.householdId = params['id'];
        this.householdName = params['name'];
      }
    );
    this.smartHomeService.retrieveAllScenarios()
      .then(scenarios => this.scenarios = scenarios)
      .then(rooms => rooms.sort(
        (scenario1, scenario2) => scenario1.name.localeCompare(scenario2.name)
      ))
      .then(() => this.isLoading = false)
      .catch(error => this.error = error.message);
  }

  onHandleError() {
    this.error = undefined;
    this.route.navigate(['main', this.householdId, this.householdName]);
  }

  activateScenario(id: string) {
    this.smartHomeService.activateScenario(id);
  }

}
