import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SmartHomeService} from "../services/smart-home.service";
import {Group} from "./group";
import {Subscription, timer} from "rxjs";
import {DeviceDto} from "../devices/deviceDto";

@Component({
  selector: 'app-group',
  template: `
    <div id="wrapper1">
      <div class="displayed"></div>
      <div id="wrapper2" *ngIf="!isLoading && !error">
        <div id="title-and-ai">
          <div id="title-with-favourite">
            <div id="group-name">
              {{ group.name }}
            </div>
            <button id="button-favourite" (click)="addRemoveFavourite()">{{ getFavouriteText(group.favourite) }}
            </button>
          </div>
          <div [ngClass]="{'ai-analyses-header': showAiAnalyses}">
            <span [ngClass]="{'ai-analyses-title': !showAiAnalyses}">AI analyses</span>
            <span *ngIf="!showAiAnalyses" class="expand-ai" id="red-color" (click)="getAiAnalyses()">⤵</span>
            <span *ngIf="showAiAnalyses" class="expand-ai" id="black-color" (click)="closeAiAnalyses()">⤴</span>
          </div>
          <div id="ai-analyses" *ngIf="showAiAnalyses">
            <div *ngIf="!isAiLoading && !isAiError" class="ai-analyses-text"> {{ aiAnalyses }}</div>
            <div *ngIf="isAiLoading" class="loader-outside">
              <div class="loader"></div>
            </div>
            <div *ngIf="isAiError" class="ai-analyses-text">
              Oops! Seems like assistant is dreaming of electric sheep right now. Try again pls.
              <button id="unset-button" (click)="unsetErrorAndShowAiAnalyses()">Got it</button>
            </div>
          </div>
        </div>
        <mat-tab-group>
          <mat-tab label="Info">
            <ul id="info-list"
                [ngClass]="{
            'color-lightgrey':group.state === 'online' || group.state === 'split',
            'color-darkgrey':group.state === 'offline' || group.state === 'unknown'}">
              <li class="info" *ngIf="group.aliases.length != 0">
                <ul id="other-names" [ngStyle]="{'color': group.state === 'online' ? 'lightgrey':'darkgrey'}"
                > Other device names
                  <span *ngIf="showAliases" id="expand" (click)="showGroupAliases()">⤵</span>
                  <span *ngIf="!showAliases" id="expand" (click)="showGroupAliases()">⤴</span>
                  <div *ngIf="showAliases">
                    <li *ngFor="let name of group.aliases">
                      <div class="value">{{ name }}</div>
                    </li>
                  </div>
                </ul>
              </li>
              <li class="info">Household name
                <div>
                  <a class="value-link" [routerLink]="['/household', this.householdId , this.householdName]"
                     [ngStyle]="{'color': group.state === 'online' || group.state === 'split' ? 'lightgrey':'darkgrey'}">
                    {{ this.householdName }}
                  </a>
                </div>
              </li>
              <li class="info">Devices
                <div class="value-device" *ngFor="let device of devicesDto">
                  <a id="device-name-1"
                     [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}"
                     [routerLink]="['/device', device.id, this.householdId , this.householdName]">
                    {{ device.name }}
                  </a>
                  <div class="switch" *ngIf="device.state === 'online'">
                    <label>
                      <input type="checkbox" [checked]="getStatusDevice(device) === 'true'"
                             (change)="turnOnOffDevice(device)">
                      <span class="slider round"></span>
                    </label>
                  </div>
                  <div *ngIf="device.state != 'online'">
                    <div id="device-offline-message">Offline</div>
                  </div>
                </div>
              </li>
              <li class="info">Type
                <div class="value"
                     [ngStyle]="{'color': (group.state === 'online' || group.state === 'split') ? 'lightgrey':'darkgrey'}">{{ getGroupType(group.type) }}
                </div>
              </li>
              <li class="info">State
                <div class="value"
                     [ngStyle]="{'color': (group.state === 'online' || group.state === 'split') ? 'lightgrey':'orange'}">{{ group.state }}
                </div>
              </li>
              <li class="info">
                <ul id="other-info">
                  Other group info
                  <span *ngIf="!showOtherInfo" id="expand" (click)="showOtherGroupInfo()">⤵</span>
                  <span *ngIf="showOtherInfo" id="expand" (click)="showOtherGroupInfo()">⤴</span>
                  <div *ngIf="showOtherInfo">
                    <li class="other-info-details"> Group ID
                      <div class="other-info-value"
                           [ngStyle]="{'color': (group.state === 'online' || group.state === 'split') ? 'lightgrey':'darkgrey'}">{{ group.id }}
                      </div>
                    </li>
                  </div>
                </ul>
              </li>
            </ul>
          </mat-tab>
          <mat-tab label="Control">
            <div>
              <button id="button-on-off"
                      *ngIf="group.type != 'devices.types.sensor' &&
                      group.type != 'devices.types.smart_speaker.yandex.station.mini'"
                      [disabled]="group.state === 'offline' || group.state == 'unknown'"
                      (click)="turnOnOffGroup(group)"
                      [ngStyle]="{'background': this.getTurnOnOffButtonColor(group)}"
              >{{ getTurnOnOffButtonText(this.getStatusGroup(group)) }}
              </button>
            </div>
          </mat-tab>
        </mat-tab-group>
      </div>
      <div *ngIf="isLoading && !error" class="wifi-outside">
        <div class="wifi-inside">
          <div class="wifi"></div>
        </div>
      </div>
      <div *ngIf="error" id="error-content">
        <h2 id="error-title">Error occurred</h2>
        <p id="error-message">{{ error }}</p>
        <button id="error-button" (click)="onHandleError()">Back to start page</button>
      </div>
      <div class="displayed"></div>
    </div>
  `,
  styleUrls: ["./group.component.css", "../css-styles/ai.loader.css"]
})
export class GroupComponent implements OnInit, OnDestroy {

  group: Group = new Group();
  devicesDto: DeviceDto[] = [];
  showAliases = true;
  typeToName = new Map;
  householdName?: string;
  householdId?: string;
  subscription!: Subscription;
  subscription1!: Subscription;
  subscription2!: Subscription;
  counter = 0;
  updateStatusInProgress = false;
  currentGroupStatus = '';
  showOtherInfo = false;
  isLoading = false;
  error = null;
  showAiAnalyses = false;
  aiAnalyses = '';
  isAiLoading?: boolean;
  isAiError = false;

  constructor(private route: Router, private activatedRoute: ActivatedRoute, private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.activatedRoute.params.subscribe(
      params => {
        this.householdId = params['householdid'];
        this.householdName = params['householdname'];
        this.smartHomeService.retrieveGroupById(params['id'])
          .then(group => this.group = group)
          .then(() => this.getDevices(this.group.devices))
          .then(() => this.isLoading = false)
          .then(() => this.updateGroupStatusAndFavouriteContinuously(this.group.id))
          .then(() => this.updateDeviceStatusContinuously())
          .catch(error => {
            this.isLoading = false;
            this.error = error.message;
          })
      }
    )
  }

  updateDeviceStatusContinuously(): void {
    this.subscription2 = timer(1000, 10000).subscribe(
      () => {
        for (let deviceDto of this.devicesDto) {
          this.smartHomeService.retrieveDeviceDto(deviceDto.id)
            .then(deviceDtoReceived => this.setDeviceStatus(deviceDto, deviceDtoReceived))
        }
      }
    )
  }

  updateGroupStatusAndFavouriteContinuously(id: string): void {
    this.subscription = timer(1000, 10000).subscribe(
      () => {
        console.log('group status upd');
        this.smartHomeService.retrieveGroupById(id)
          .then(group => {
            this.group.favourite = group.favourite;
            if (!this.updateStatusInProgress) {
              this.setGroupStatus(this.group, group);
            } else {
              this.currentGroupStatus = this.getStatusGroup(group);
            }
          });
      }
    );
  }

  getAiAnalyses() {
    this.showAiAnalyses = true;
    if (!this.aiAnalyses && this.householdId) {
      this.isAiLoading = true;
      this.smartHomeService.getAiAnalysesForGroup(this.householdId, this.group.id)
        .then(chatResponse => {
          this.isAiLoading = false;
          this.aiAnalyses = chatResponse.response
        })
        .catch(() => {
          this.isAiLoading = false;
          this.isAiError = true;
        });
    }
  }

  closeAiAnalyses() {
    this.showAiAnalyses = false;
  }

  turnOnOffGroup(group: Group): void {
    this.smartHomeService.turnOnOffGroup(group.id, this.getStatusGroup(group));
    this.updateGroupStatusAfterApplyTurnOnOffButton(group);
  }

  updateGroupStatusAfterApplyTurnOnOffButton(group: Group) {
    this.updateStatusInProgress = true;
    this.setGroupStatusInverse(this.group, group);
    this.subscription1 = timer(0, 10000).subscribe(
      () => {
        console.info(`start status upd after turnOnOff, expected: ${this.getStatusGroup(this.group)}, actual: ${this.currentGroupStatus}`);
        this.counter++;
        if (this.counter > 30) {
          alert(`Note: it seems something went wrong by switching ${group.name}...`);
          this.finishUpdateGroupStatusAfterApplyTurnOnOffButton(`end status upd: timeout finished -> unsubscribe`);
          return;
        }
        if (this.getStatusGroup(this.group) === this.currentGroupStatus) {
          this.finishUpdateGroupStatusAfterApplyTurnOnOffButton(`end status upd -> unsubscribe`);
        }
      });
  }

  finishUpdateGroupStatusAfterApplyTurnOnOffButton(consoleLog: string) {
    this.updateStatusInProgress = false;
    this.subscription1.unsubscribe();
    this.counter = 0;
    console.info(consoleLog);
  }

  getGroupType(type: string): string {
    for (let [key, value] of this.getTypeToNameMap()) {
      if (key === type) {
        return `${value}`;
      }
    }
    return "";
  }

  getTypeToNameMap(): Map<string, string> {
    this.typeToName.set("devices.types.light", "Light");
    this.typeToName.set("devices.types.light.lamp", "Lamps");
    this.typeToName.set("devices.types.vacuum_cleaner", "Vacuum cleaners");
    this.typeToName.set("devices.types.media_device.tv", "TV");
    this.typeToName.set("devices.types.sensor", "Sensors");
    this.typeToName.set("devices.types.socket", "Sockets");
    this.typeToName.set("devices.types.hub", "Hubs");
    this.typeToName.set("devices.types.cooking.kettle", "Kettles");
    this.typeToName.set("devices.types.humidifier", "Humidifiers");
    this.typeToName.set("devices.types.thermostat.ac", "AC");
    this.typeToName.set("devices.types.switch", "Switches");
    this.typeToName.set("devices.types.sensor.button", "Buttons");
    this.typeToName.set("devices.types.smart_speaker.yandex.station.mini", "Speakers");
    return this.typeToName;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      console.log('destroy 0');
      this.subscription.unsubscribe();
    }
    if (this.subscription1) {
      console.log('destroy 1');
      this.subscription1.unsubscribe();
    }
    if (this.subscription2) {
      console.log('destroy 2');
      this.subscription2.unsubscribe();
    }

  }

  addRemoveFavourite() {
    console.log(`favourite ${!this.group.favourite}`);
    this.smartHomeService.updateGroup(this.group.id, '/favourite', String(!this.group.favourite));
  }

  getFavouriteText(favourite: boolean | undefined): string {
    if (favourite) {
      return 'Remove from Favourite'
    } else {
      return 'Add to Favourite'
    }
  }

  getTurnOnOffButtonText(status: string): string {
    if (status === 'true') {
      return 'ON'
    } else {
      return 'OFF'
    }
  }

  showOtherGroupInfo() {
    this.showOtherInfo = !this.showOtherInfo;
  }

  showGroupAliases() {
    this.showAliases = !this.showAliases;
  }

  onHandleError() {
    this.error = null;
    this.route.navigate(['main', this.householdId, this.householdName]);
  }

  private getDevices(ids: string[]) {
    for (let id of ids) {
      this.smartHomeService.retrieveDeviceDto(id)
        .then(deviceDto => this.devicesDto.push(deviceDto));
    }
  }

  turnOnOffDevice(device: DeviceDto): void {
    this.smartHomeService.turnOnOffDevice(device.id, this.getStatusDevice(device));
  }

  getStatusGroup(group: Group): string {
    for (let capability of group.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        return capability.state.value;
      }
    }
    return '';
  }

  setGroupStatus(groupToUpd: Group, groupReceived: Group) {
    let newStatus = this.getStatusGroup(groupReceived);
    for (let capability of groupToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  setGroupStatusInverse(groupToUpd: Group, groupReceived: Group) {
    let newStatus;
    if (this.getStatusGroup(groupReceived) === 'true') {
      newStatus = 'false';
    } else {
      newStatus = 'true';
    }
    for (let capability of groupToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  getStatusDevice(device: DeviceDto): string {
    for (let capability of device.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        return capability.state.value;
      }
    }
    return '';
  }

  setDeviceStatus(deviceToUpd: DeviceDto, deviceReceived: DeviceDto) {
    let newStatus = this.getStatusDevice(deviceReceived);
    for (let capability of deviceToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  getTurnOnOffButtonColor(group: Group): string {
    if (this.getStatusGroup(group) === 'true' && (group.state === 'online' || group.state === 'split')) {
      return 'green';
    }
    if (this.getStatusGroup(group) === 'false' && (group.state === 'online' || group.state === 'split')) {
      return 'red';
    }
    return 'lightgrey';
  }

  unsetErrorAndShowAiAnalyses() {
    this.isAiError = false;
    this.showAiAnalyses = false;
  }

}
