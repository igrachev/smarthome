import {Capability} from "../models/capability";

export class Group {

  id = '';
  name = '';
  household_id = '';
  type = '';
  state = '';
  favourite?: boolean;
  aliases: string [] = [];
  devices: string [] = [];
  capabilities: Capability[] = [];
}
