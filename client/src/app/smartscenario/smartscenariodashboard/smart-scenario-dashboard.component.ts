import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Subscription, timer} from "rxjs";
import {SmartScenario} from "../../models/smartscenario";
import {SmartHomeService} from "../../services/smart-home.service";

@Component({
  selector: 'app-smart-scenario-header',
  template: `
    <div id="wrapper">
      <ul id="smart-scenarios"
          [ngClass]="{'smart-scenarios-row': !isArrowDown, 'smart-scenarios-column': isArrowDown}"
          [ngStyle]="{'padding': smartScenarios.length === 0 ? '0 0.5rem 0.5rem 0':'0 0.5rem 0.5rem 0.5rem'}"
      >
        <li id="smart-scenario"
            *ngFor="let smartScenario of smartScenarios | slice:0:limit">
          {{ smartScenario.name | shortenSmartScenarioName}}
          <span class="control-button" *ngIf="smartScenario.active"
                (click)="deactivateSmartScenario(smartScenario)">⏹️</span>
          <span class="control-button" *ngIf="!smartScenario.active"
                (click)="activateSmartScenario(smartScenario)">▶</span>
        </li>
        <div id="manage-and-arrow">
          <a *ngIf="smartScenarios.length === 0 || isArrowDown"
             id="manage-smart-scenarios" [routerLink]="['/smart-scenarios', this.householdId]"
             (click)="setLimitToThreeAndArrowDownFalse()">
            {{ this.getManageSmartScenarioText() }}
          </a>
          <img *ngIf="smartScenarios.length > 0 && !isArrowDown" class="arrow" src="assets/down-arrow-red.svg"
               (click)="unsetLimitAndArrowDownTrue()"/>
          <img *ngIf="isArrowDown" class="arrow" src="assets/up-arrow-red.svg"
               (click)="setLimitToThreeAndArrowDownFalse()"/>
        </div>
      </ul>
    </div>
  `,
  styleUrls: ["./smart-scenario-dashboard.component.css"]
})
export class SmartScenarioDashboardComponent implements OnInit {
  subscription!: Subscription;
  subscription1!: Subscription;
  isArrowDown = false;
  smartScenarios: SmartScenario[] = [];
  @Input() householdId?: string;
  limit = 3;

  constructor(private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.updateSmartScenarios();
    this.startSmartScenarios();
  }

  updateSmartScenarios() {
    this.subscription = timer(1000, 60000).subscribe(() => this.getSmartScenarios())
  }

  startSmartScenarios() {
    this.subscription1 = timer(1000, 60000).subscribe(
      () => {
        this.smartScenarios.forEach(scenario => {
          if(scenario.id && scenario.active) {
            console.log("Start smart scenario!!!" + scenario.name);
            this.smartHomeService.startSmartScenario(scenario.id)
              .then(smartScenarioReceived => scenario = smartScenarioReceived);
          }
        })
      }
    )
  }

  activateSmartScenario(smartScenario: SmartScenario) {
    if (smartScenario.id) {
      this.smartHomeService.activateSmartScenario(smartScenario.id)
        .then(smartScenarioReceived => smartScenario.active = smartScenarioReceived.active);
    }
  }

  deactivateSmartScenario(smartScenario: SmartScenario) {
    if (smartScenario.id) {
      this.smartHomeService.deactivateSmartScenario(smartScenario.id)
        .then(smartScenarioReceived => smartScenario.active = smartScenarioReceived.active);
    }
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes['householdId'] && !changes['householdId'].isFirstChange()) {
      this.getSmartScenarios();
    }
  }

  getSmartScenarios() {
    if (this.householdId) {
      this.smartHomeService.retrieveAllSmartScenarios(this.householdId)
        .then(smartScenarios => this.smartScenarios = smartScenarios);
    }
  }

  getManageSmartScenarioText() {
    if (this.smartScenarios.length > 0) {
      return "Manage Smart Scenarios";
    }
    return "Create new Smart Scenario";
  }

  unsetLimitAndArrowDownTrue() {
    this.isArrowDown = true;
    console.log(`hey ` + this.smartScenarios.length);
    this.limit = this.smartScenarios.length;
  }

  setLimitToThreeAndArrowDownFalse() {
    this.isArrowDown = false;
    this.limit = 3;
  }
}
