import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SmartHomeService} from "../../services/smart-home.service";
import {SmartScenario} from "../../models/smartscenario";
import {Room} from "../../rooms/room";
import {Subscription, timer} from "rxjs";

@Component({
  selector: 'app-smart-scenario',
  template: `
    <div id="wrapper1">
      <div class="displayed"></div>
      <div id="wrapper2" *ngIf="!isLoading && !error">
        <p id="scenarioTitle">Smart Scenarios</p>
        <div id="new-smart-scenario">
          <p *ngIf="!plusApplied" (click)="setPlusApplied()" id="plus-sign">
            <span id="plus">+</span>
          </p>
          <div *ngIf="plusApplied && !isCreateLoading && !isErrorSmartScenario">
            <div id="description-and-arrow-up">
              <div id="new-scenario-description">
                <textarea [(ngModel)]="newScenarioDescription"
                          placeholder="Provide description of a new scenario and select a room from suggestions below"></textarea>
              </div>
              <img class="arrow-new-scenario" src="assets/up-arrow-lightgrey.svg" (click)="collapseNewSmartScenario()"/>
            </div>
            <div id="rooms-and-create-button">
              <div id="rooms">
                <li *ngFor="let room of rooms" id="room-suggestion" (click)="selectRoom(room.id)">
                  <span [ngClass]="{'colorful': roomId === room.id}">{{ room.name }}</span>
              </div>
              <button id="create-button" (click)="createSmartScenario()"
                      [disabled]="!roomId || !newScenarioDescription"
                      [ngClass]="{'glowing': roomId && newScenarioDescription}">
                <span
                  [ngClass]="{'colorful-and-pointer': roomId && newScenarioDescription}"> Create Smart Scenario </span>
              </button>
            </div>
          </div>
          <div *ngIf="isCreateLoading" class="loader-outside">
            <div class="loader"></div>
          </div>
          <div *ngIf="isErrorSmartScenario" id="smart-scenario-error">
            <div id="smart-scenario-error-text">Oops, seems like AI Assistant is dreaming of electric sheep now. Try again</div>
            <img class="arrow-new-scenario" src="assets/up-arrow-lightgrey.svg" (click)="collapseNewSmartScenario()"/>
          </div>
        </div>
        <ul>
          <li *ngFor="let smartScenario of smartScenarios">
            <div id="smart-scenario-block">
              <div id="smart-scenario-name-and-buttons">
                <span id="smart-scenario-name">{{ smartScenario.name }}</span>
                <div id="control-buttons">
                  <img class="control-button" *ngIf="smartScenario.active"
                       (click)="deactivateSmartScenario(smartScenario)" src="assets/stop.svg"/>
                  <img class="control-button" *ngIf="!smartScenario.active"
                       (click)="activateSmartScenario(smartScenario)" src="assets/activate.svg"/>
                  <img id="delete-icon" (click)="deleteSmartScenario(smartScenario)" src="assets/trash-delete.svg"/>
                  <img class="arrow" src="assets/up-arrow-lightgrey.svg" *ngIf="expandedScenarioId === smartScenario.id"
                       (click)="expand(smartScenario.id)"/>
                  <img class="arrow" src="assets/down-arrow-lightgrey.svg"
                       *ngIf="expandedScenarioId != smartScenario.id"
                       (click)="expand(smartScenario.id)"/>
                </div>
              </div>
              <div id="room-and-description">
                <div id="room"> {{ getRoomName(smartScenario.roomId) }}</div>
                <div id="description"> {{ smartScenario.prompt }}</div>
              </div>
              <div *ngIf="expandedScenarioId === smartScenario.id">
                <table *ngIf="smartScenario.actions.length != 0" id="actions">
                  <tbody>
                  <tr id="action" *ngFor="let action of sortActionsDescending(smartScenario.actions)">
                    <td id="action-date"> {{ action.date | date: 'dd MMM HH:mm:ss' }}</td>
                    <div>
                      <td id="action-device" *ngFor="let deviceAction of action.deviceActions">
                        <div id="device-action-device"> {{ deviceAction.device }}</div>
                        <div id="device-action-state"> {{ deviceAction.instance }}: {{ deviceAction.value }}</div>
                        <div id="device-action-comment"> {{ deviceAction.comment }}</div>
                      </td>
                      <td id="action-comment"> {{ action.comment }}</td>
                    </div>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div *ngIf="isLoading && !error" class="wifi-outside">
        <div class="wifi-inside">
          <div class="wifi"></div>
        </div>
      </div>
      <div *ngIf="error" id="error-content">
        <h2 id="error-title">Error occurred</h2>
        <p id="error-message">{{ error }}</p>
        <button id="error-button" (click)="onHandleError()">Back to start page</button>
      </div>
      <div class="displayed"></div>
    </div>
  `,
  styleUrls: ["./smart-scenario.component.css", "../../css-styles/wi-fi.loader.css", "../../css-styles/error.css"]
})
export class SmartScenarioComponent implements OnInit {

  subscription!: Subscription;
  roomIdToNameMap = new Map<string, string>();
  householdId?: string;
  rooms: Room[] = [];
  smartScenarios: SmartScenario[] = []
  expandedScenarioId?: number;
  plusApplied = false;
  newScenarioDescription?: string;
  roomId?: string;
  isCreateLoading = false;
  isLoading?: boolean;
  error?: string;
  isErrorSmartScenario?: boolean;

  constructor(private route: Router, private activatedRoute: ActivatedRoute, private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.activatedRoute.params.subscribe(
      params => {
        this.householdId = params['householdid'];
      }
    );
    if (this.householdId) {
      this.smartHomeService.retrieveAllRoomsByHouseholdId(this.householdId)
        .then(rooms => this.rooms = rooms)
        .then(rooms => rooms.forEach(room => this.roomIdToNameMap.set(room.id, room.name)));
      this.smartHomeService.retrieveAllSmartScenarios(this.householdId)
        .then(scenarios => this.smartScenarios = scenarios)
        .then(() => this.isLoading = false)
        .catch(error => this.error = error.message);
      this.constantlyUpdateSmartScenarios();
    }
  }

  constantlyUpdateSmartScenarios() {
    this.subscription = timer(1000, 15000).subscribe(
      () => {
        if (this.householdId) {
          this.smartHomeService.retrieveAllSmartScenarios(this.householdId)
            .then(scenarios => this.smartScenarios = scenarios);
        }
      }
    )
  }

  sortActionsDescending(actions: any[]): any[] {
    return actions.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
  }


  activateSmartScenario(smartScenario: SmartScenario) {
    if (smartScenario.id) {
      this.smartHomeService.activateSmartScenario(smartScenario.id)
        .then(smartScenarioReceived => smartScenario.active = smartScenarioReceived.active);
    }
  }

  deactivateSmartScenario(smartScenario: SmartScenario) {
    if (smartScenario.id) {
      this.smartHomeService.deactivateSmartScenario(smartScenario.id)
        .then(smartScenarioReceived => smartScenario.active = smartScenarioReceived.active);
    }
  }

  deleteSmartScenario(smartScenario: SmartScenario) {
    if (smartScenario.id) {
      this.smartHomeService.deleteSmartScenario(smartScenario.id)
        .then(() => this.smartScenarios = this.smartScenarios.filter(s => s.id !== smartScenario.id));
    }
  }

  getRoomName(roomId: string): string {
    // @ts-ignore
    for (let [key, value] of this.roomIdToNameMap) {
      if (key === roomId) {
        return `${value}`;
      }
    }
    return '';
  }

  onHandleError() {
    this.error = undefined;
    this.route.navigate(['main', this.householdId]);
  }

  expand(scenarioId?: number) {
    if (scenarioId && this.expandedScenarioId === scenarioId) {
      this.expandedScenarioId = undefined;
    } else {
      this.expandedScenarioId = scenarioId;
    }
  }

  setPlusApplied() {
    this.plusApplied = true;
  }

  selectRoom(id: string) {
    if (!this.roomId) {
      this.roomId = id;
    } else {
      this.roomId = undefined;
    }
  }

  collapseNewSmartScenario() {
    this.roomId = undefined;
    this.newScenarioDescription = undefined;
    this.plusApplied = false;
    this.isCreateLoading = false;
    this.isErrorSmartScenario = false;
  }

  createSmartScenario() {
    if (this.householdId && this.roomId && this.newScenarioDescription) {
      this.isCreateLoading = true;
      this.smartHomeService.createSmartScenario(this.householdId, this.roomId, this.newScenarioDescription)
        .then(smartScenario => this.smartScenarios.push(smartScenario))
        .then(() => {
          this.newScenarioDescription = undefined;
          this.roomId = undefined;
          this.plusApplied = false;
          this.isCreateLoading = false;
        })
        .catch(() => {
          this.isErrorSmartScenario = true;
          this.isCreateLoading = false;
        });
    }
  }
}
