import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SmartHomeService} from "../services/smart-home.service";
import {Message} from "../models/message";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-chat',
  template: `
    <div class="chat-container" *ngIf="mainIsLoading === false">
      <div id="chat-header">
        <div id="arrow">
          <span (click)="sendChatOpenedFalse()">
          <img id="down-arrow" src="assets/down-arrow.svg"/>
          </span>
        </div>
      </div>
      <div class="messages" #messagesContainer>
        <div *ngFor="let message of messages">
          <p id="message-text"
             [ngClass]="{'sent': message.sender === 'user', 'received': message.sender !== 'user'}">{{ message.text }}</p>
          <button id="suggestion" *ngFor="let suggestion of message.suggestions" (click)="sendMessage(suggestion)">
            {{ suggestion }}
          </button>
        </div>
        <div *ngIf="isLoading && !isError" class="loader-outside">
          <div class="loader"></div>
        </div>
        <div *ngIf="isError" id="error-message">
          Oops! Seems like assistant is dreaming of electric sheep right now. Try again pls.
          <button id="unset-button" (click)="unsetErrorAndRemoveLastMessage()">Got it</button>
        </div>
      </div>
      <div class="input-area">
        <textarea [(ngModel)]="newMessage" (keyup.enter)="sendMessage()" placeholder="Message AI Assistant"></textarea>
        <button *ngIf="newMessage" id="send-button" (click)="sendMessage()">
          <img id="send-icon" src="assets/message-icon.svg"/>
        </button>
      </div>
    </div>
  `,
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @ViewChild('messagesContainer') private messagesContainer!: ElementRef;
  @Input() mainIsLoading?: boolean;
  @Input() householdId: string | undefined;
  @Output() chatOpened = new EventEmitter<boolean>();
  isLoading: boolean = false;
  isError: boolean = false;
  messages: Message[] = [];
  newMessage = '';
  previousMessageNumber = 0;

  constructor(private smartHomeService: SmartHomeService) {
  }

  ngOnInit() {
    const newMessage: Message = {
      text: `Hello ${environment.authUsername}! I'm your AI assistant, please ask me anything about this home
      or just start with quick suggestions below.`,
      sender: 'chat-gpt',
      suggestions: ['Provide general info', 'Devices batteries status']
    };
    this.messages.push(newMessage);
  }

  sendMessage(messageText?: string) {
    this.isLoading = true;
    const textToSend = messageText || this.newMessage.trim();
    if (textToSend) {
      const newMessageSent: Message = {
        text: textToSend,
        sender: 'user',
        suggestions: []
      };
      this.messages.push(newMessageSent);
      if (this.householdId) {
        this.smartHomeService.sendMessageToChat(this.householdId, textToSend)
          .then(chatResponse => {
            const newMessageReceived: Message = {
              text: chatResponse.response,
              sender: 'chat-gpt',
              suggestions: []
            }
            this.isLoading = false;
            this.newMessage = '';
            this.messages.push(newMessageReceived);
          })
          .catch(() => {
            this.isLoading = false;
            this.isError = true;
            this.newMessage = '';
          });
      }
    }
  }

  sendChatOpenedFalse() {
    this.chatOpened.emit(false);
  }

  private scrollToBottom(): void {
    try {
      this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  ngAfterViewChecked() {
    if (this.messages.length > this.previousMessageNumber) {
      this.scrollToBottom();
      this.previousMessageNumber = this.messages.length;
    }
  }

  unsetErrorAndRemoveLastMessage() {
    this.isError = false;
    this.messages.pop();
  }
}
