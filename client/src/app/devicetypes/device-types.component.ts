import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-device-types',
  template: `
      <div *ngIf="mainIsLoading === false">
          <ul id="allTypes">
              <li (click)="selectType('allActiveDevices')" [ngStyle]="{'border-bottom': allActiveDevicesSelected ? '2px solid lightgrey':'black'}">All active</li>
              <li *ngFor="let type of deviceTypes" (click)="selectType(type)"
                  [ngStyle]="{'border-bottom': typeToSelected.get(type) ? '2px solid lightgrey':'black'}">{{getDeviceNameByType(type)}}</li>
          </ul>
      </div>
  `,
  styleUrls: ["./device-types.component.css"]
})
export class DeviceTypesComponent implements OnChanges {

  @Output() typeSelected = new EventEmitter<string>();
  @Input() deviceTypes?: Set<string> = new Set<string>();
  @Input() mainIsLoading?: boolean;
  allActiveDevicesSelected = false;
  typeToName = new Map;
  typeToSelected: Map<string, boolean> = new Map;

  getTypeToNameMap(): Map<string, string> {
    this.typeToName.set("devices.types.light", "Light");
    this.typeToName.set("devices.types.light.lamp", "Lamps");
    this.typeToName.set("devices.types.vacuum_cleaner", "Vacuum cleaners");
    this.typeToName.set("devices.types.media_device.tv", "TV");
    this.typeToName.set("devices.types.sensor", "Sensors");
    this.typeToName.set("devices.types.socket", "Sockets");
    this.typeToName.set("devices.types.hub", "Hubs");
    this.typeToName.set("devices.types.cooking.kettle", "Kettles");
    this.typeToName.set("devices.types.humidifier", "Humidifiers");
    this.typeToName.set("devices.types.thermostat.ac", "AC");
    this.typeToName.set("devices.types.switch", "Switches");
    this.typeToName.set("devices.types.sensor.button", "Buttons");
    this.typeToName.set("devices.types.smart_speaker.yandex.station.mini", "Speakers");
    return this.typeToName;
  }

  selectType(type: string): void {
    if(type === 'allActiveDevices') {
      this.allActiveDevicesSelected = true;
      // @ts-ignore
      for (let value of this.deviceTypes) {
          this.typeToSelected.set(value, false);
      }
    } else {
      this.allActiveDevicesSelected = false;
      // @ts-ignore
      for (let value of this.deviceTypes) {
        if (value === type) {
          this.typeToSelected.set(value, true);
        } else {
          this.typeToSelected.set(value, false);
        }
      }
    }
    this.typeSelected.emit(type);
  }

  getDeviceNameByType(type: string): string {
    for (let [key, value] of this.getTypeToNameMap()) {
      if (key === type) {
        return `${value}`;
      }
    }
    return "";
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.info(`!!!onchanges device-types started`);
    if (this.deviceTypes) {
      for (let value of this.deviceTypes) {
        this.typeToSelected.set(value, false);
      }
    }
  }

}
