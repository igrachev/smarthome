import {Parameter} from "./parameter";
import {State} from "./state";

export class Property {
  type = '';
  parameters: Parameter = new Parameter();
  state: State = new State();
  last_updated?: Date;
}
