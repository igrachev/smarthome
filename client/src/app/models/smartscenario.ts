import {SmartScenarioAction} from "./smartscenarioaction";

export class SmartScenario {
  id?: number;
  name = '';
  prompt = '';
  roomId = '';
  householdId = '';
  active?: boolean;
  actions: SmartScenarioAction[] = [];
}
