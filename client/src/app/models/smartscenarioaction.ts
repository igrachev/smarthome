import {DeviceAction} from "./deviceaction";

export class SmartScenarioAction{
  deviceActions: DeviceAction[] = [];
  comment = '';
  date?: Date;
}
