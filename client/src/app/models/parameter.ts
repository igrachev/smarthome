import {Mode} from "./mode";
import {Range} from "./range";
import {Temperature} from "./temperature";
import {Event} from "./event";

export class Parameter {
  split = '';
  color_model = '';
  instance = '';
  unit = '';
  random_access = '';
  looped = '';
  range: Range = new Range();
  temperature_k: Temperature = new Temperature();
  modes: Mode[] = [];
  events: Event[] = [];
}
