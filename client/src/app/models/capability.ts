import {Parameter} from "./parameter";
import {State} from "./state";

export class Capability {
  type = '';
  parameters: Parameter = new Parameter();
  state: State = new State();
}
