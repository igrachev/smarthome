import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Device} from "../device";
import {SmartHomeService} from "../../services/smart-home.service";
import {Subscription, timer} from "rxjs";

@Component({
  selector: 'app-device',
  template: `
    <div id="wrapper1">
      <div class="displayed"></div>
      <div id="wrapper2" *ngIf="isLoading === false && !error">
        <div id="title-and-ai">
          <div id="title-with-favourite">
            <div id="device-name">
              {{ device.name }}
            </div>
            <button id="button-favourite" (click)="addRemoveFavourite()">{{ getFavouriteText(device.favourite) }}
            </button>
          </div>
          <div [ngClass]="{'ai-analyses-header': showAiAnalyses}">
            <span [ngClass]="{'ai-analyses-title': !showAiAnalyses}">AI analyses</span>
            <span *ngIf="!showAiAnalyses" class="expand-ai" id="red-color" (click)="getAiAnalyses()">⤵</span>
            <span *ngIf="showAiAnalyses" class="expand-ai" id="black-color" (click)="closeAiAnalyses()">⤴</span>
          </div>
          <div id="ai-analyses" *ngIf="showAiAnalyses">
            <div *ngIf="!isAiLoading && !isAiError" class="ai-analyses-text"> {{ aiAnalyses }}</div>
            <div *ngIf="isAiLoading" class="loader-outside">
              <div class="loader"></div>
            </div>
            <div *ngIf="isAiError" class="ai-analyses-text">
              Oops! Seems like assistant is dreaming of electric sheep right now. Try again pls.
              <button id="unset-button" (click)="unsetErrorAndShowAiAnalyses()">Got it</button>
            </div>
          </div>
        </div>
        <mat-tab-group>
          <mat-tab label="Info">
            <ul id="info-list"
                [ngClass]="{
            'color-lightgrey':device.state === 'online',
            'color-darkgrey':device.state === 'offline' || device.state === 'unknown'
        }">
              <li class="info" *ngIf="device.aliases.length != 0">
                <ul id="other-names"> Other device names
                  <span *ngIf="showAliases" id="expand" (click)="showDeviceAliases()">⤴</span>
                  <span *ngIf="!showAliases" id="expand" (click)="showDeviceAliases()">⤵</span>
                  <div *ngIf="showAliases">
                    <li *ngFor="let name of device.aliases">
                      <div class="value">{{ name }}</div>
                    </li>
                  </div>
                </ul>
              </li>
              <li class="info">Household name
                <div>
                  <a class="value-link"
                     [routerLink]="['/household', this.householdId , this.householdName]"
                     [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">
                    {{ this.householdName }}
                  </a>
                </div>
              </li>
              <li class="info">Room
                <div class="value"
                     [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">{{ roomName }}
                </div>
              </li>
              <li class="info">Type
                <div class="value"
                     [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">{{ getDeviceNameByType(device.type) }}
                </div>
              </li>
              <li class="info">State
                <div class="value"
                     [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'orange'}">{{ device.state }}
                </div>
              </li>
              <li class="info">Groups
                <div *ngIf="device.groups.length != 0">
                  <div *ngFor="let groupId of device.groups">
                    <div>
                      <a class="value-link"
                         [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}"
                         [routerLink]="['/group', groupId, this.householdId , this.householdName]">
                        {{ getGroupNameById(groupId) }}
                      </a>
                    </div>
                  </div>
                </div>
                <div *ngIf="device.groups.length === 0">
                  <div class="value" [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">No
                    groups
                  </div>
                </div>
              </li>
              <li class="info">
                <ul id="other-info">
                  <div id="other-info-text">
                    <div>Other device info</div>
                    <span *ngIf="!showOtherInfo" id="expand" (click)="showOtherDeviceInfo()">⤵</span>
                    <span *ngIf="showOtherInfo" id="expand" (click)="showOtherDeviceInfo()">⤴</span>
                  </div>
                  <div *ngIf="showOtherInfo">
                    <li class="other-info-details"> Device ID
                      <div class="other-info-value"
                           [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">
                        {{ device.id }}
                      </div>
                    </li>
                    <li class="other-info-details"> Device external ID
                      <div class="other-info-value"
                           [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">
                        {{ device.external_id }}
                      </div>
                    </li>
                    <li class="other-info-details"> Device skill ID
                      <div class="other-info-value"
                           [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">
                        {{ device.skill_id }}
                      </div>
                    </li>
                  </div>
                </ul>
              </li>
            </ul>
          </mat-tab>
          <mat-tab label="Control">
            <app-sensortype
              *ngIf="device.type === 'devices.types.sensor'"
              [device]="device"
            ></app-sensortype>
            <!--            button should be removed after control for all devices is implemented-->
            <button id="button-on-off"
                    *ngIf="device.type != 'devices.types.sensor' &&
                            device.type != 'devices.types.smart_speaker.yandex.station.mini'"
                    [disabled]="device.state !== 'online'"
                    (click)="turnOnOffDevice(device)"
                    [ngStyle]="{'background': this.getTurnOnOffButtonColor(device)}"
            >{{ getTurnOnOffButtonText(this.getStatusDevice(device)) }}
            </button>
          </mat-tab>
        </mat-tab-group>
      </div>
      <div *ngIf="isLoading === true && !error" class="wifi-outside">
        <div class="wifi-inside">
          <div class="wifi"></div>
        </div>
      </div>
      <div *ngIf="error" id="error-content">
        <h2 id="error-title">Error occurred</h2>
        <p id="error-message">{{ error }}</p>
        <button id="error-button" (click)="onHandleError()">Back to start page</button>
      </div>
      <div class="displayed"></div>
    </div>
  `,
  styleUrls: ["./device.component.css", "../../css-styles/ai.loader.css"]
})
export class DeviceComponent implements OnInit, OnDestroy {

  device: Device = new Device();
  householdName?: string;
  householdId?: string;
  roomName?: string;
  typeToName = new Map;
  groupIdToNames = new Map<string, string>();
  subscription!: Subscription;
  subscription1!: Subscription;
  showOtherInfo = false;
  updateStatusInProgress = false;
  currentDeviceStatus = '';
  showAliases = true;
  showAiAnalyses = false;
  aiAnalyses = '';
  isAiLoading?: boolean;
  counter = 0;
  isLoading?: boolean;
  error = null;
  isAiError = false;

  constructor(private route: Router, private activatedRoute: ActivatedRoute, private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    console.log(`ngOnInit single dev`);
    this.isLoading = true;
    this.activatedRoute.params.subscribe(
      params => {
        this.householdId = params['householdid'];
        this.householdName = params['householdname'];
        this.smartHomeService.retrieveDeviceById(params['id'])
          .then(device => this.device = device)
          .then(device => {
            this.getRoomName(device.room);
            this.getGroupNames(device.groups)
          })
          .then(() => this.isLoading = false)
          .then(() => this.updateDeviceStatusAndFavouriteContinuously(this.device.id))
          .catch(error => {
              this.isLoading = false;
              this.error = error.message;
            }
          );
      }
    );
  }

  getTurnOnOffButtonColor(device: Device): string {
    if (this.getStatusDevice(device) === 'true' && device.state === 'online') {
      return 'green';
    }
    if (this.getStatusDevice(device) === 'false' && device.state === 'online') {
      return 'red';
    }
    return 'lightgrey';
  }

  getRoomName(roomId: string) {
    this.smartHomeService.retrieveRoomById(roomId)
      .then(room => this.roomName = room.name);
  }

  updateDeviceStatusAndFavouriteContinuously(id: string): void {
    this.subscription = timer(1000, 10000).subscribe(
      () => {
        this.smartHomeService.retrieveDeviceById(id)
          .then(device => {
            console.log(`NgOnInit favourite ${this.device.name} actual ${this.device.favourite} from bd ${device.favourite}`);
            this.device.favourite = device.favourite;
            if (!this.updateStatusInProgress) {
              this.setDeviceStatus(this.device, device);
            } else {
              this.currentDeviceStatus = this.getStatusDevice(device);
            }
          });
      }
    );
  }

  turnOnOffDevice(device: Device): void {
    this.smartHomeService.turnOnOffDevice(device.id, this.getStatusDevice(device));
    this.updateDeviceStatusAfterApplyTurnOnOffButton(device);
  }

  updateDeviceStatusAfterApplyTurnOnOffButton(device: Device) {
    this.updateStatusInProgress = true;
    this.setDeviceStatusInverse(this.device, device);
    this.subscription1 = timer(0, 10000).subscribe(
      () => {
        console.info(`start status upd after turnOnOff, expected: ${this.getStatusDevice(this.device)}, actual: ${this.currentDeviceStatus}`);
        this.counter++;
        if (this.counter > 30) {
          alert(`Note: it seems something went wrong by switching ${device.name}...`);
          this.finishUpdateDeviceStatusAfterApplyTurnOnOffButton(`end status upd: timeout finished -> unsubscribe`);
          return;
        }
        if (this.getStatusDevice(this.device) === this.currentDeviceStatus) {
          this.finishUpdateDeviceStatusAfterApplyTurnOnOffButton(`end status upd -> unsubscribe`);
        }
      });
  }

  finishUpdateDeviceStatusAfterApplyTurnOnOffButton(consoleLog: string) {
    this.updateStatusInProgress = false;
    this.subscription1.unsubscribe();
    this.counter = 0;
    console.info(consoleLog);
  }

  showOtherDeviceInfo() {
    this.showOtherInfo = !this.showOtherInfo;
  }

  getAiAnalyses() {
    this.showAiAnalyses = true;
    if (!this.aiAnalyses && this.householdId) {
      this.isAiLoading = true;
      this.smartHomeService.getAiAnalysesForDevice(this.householdId, this.device.id)
        .then(chatResponse => {
          this.isAiLoading = false;
          this.aiAnalyses = chatResponse.response
        })
        .catch(() => {
          this.isAiLoading = false;
          this.isAiError = true;
        });
    }
  }

  closeAiAnalyses() {
    this.showAiAnalyses = false;
  }

  showDeviceAliases() {
    this.showAliases = !this.showAliases;
  }

  getDeviceNameByType(type: string): string {
    for (let [key, value] of this.getTypeToNameMap()) {
      if (key === type) {
        return `${value}`;
      }
    }
    return "";
  }

  getTypeToNameMap(): Map<string, string> {
    this.typeToName.set("devices.types.light", "Light");
    this.typeToName.set("devices.types.light.lamp", "Lamps");
    this.typeToName.set("devices.types.vacuum_cleaner", "Vacuum cleaners");
    this.typeToName.set("devices.types.media_device.tv", "TV");
    this.typeToName.set("devices.types.sensor", "Sensors");
    this.typeToName.set("devices.types.socket", "Sockets");
    this.typeToName.set("devices.types.hub", "Hubs");
    this.typeToName.set("devices.types.cooking.kettle", "Kettles");
    this.typeToName.set("devices.types.humidifier", "Humidifiers");
    this.typeToName.set("devices.types.thermostat.ac", "AC");
    this.typeToName.set("devices.types.switch", "Switches");
    this.typeToName.set("devices.types.sensor.button", "Buttons");
    this.typeToName.set("devices.types.smart_speaker.yandex.station.mini", "Speakers");
    return this.typeToName;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  addRemoveFavourite() {
    this.smartHomeService.updateDevice(this.device.id, '/favourite', String(!this.device.favourite));
  }

  getFavouriteText(favourite: boolean | undefined): string {
    if (favourite) {
      return 'Remove from Favourite'
    } else {
      return 'Add to Favourite'
    }
  }

  getTurnOnOffButtonText(status: string): string {
    if (status === 'true') {
      return 'ON'
    } else {
      return 'OFF'
    }
  }

  onHandleError() {
    this.error = null;
    this.route.navigate(['main', this.householdId, this.householdName]);
  }

  getGroupNames(groupIds: string[]): void {
    if (groupIds.length != 0) {
      for (let id of groupIds) {
        this.smartHomeService.retrieveGroupById(id)
          .then(group => this.groupIdToNames.set(group.id, group.name));
      }
    }
  }

  getGroupNameById(id: string): string {
    for (let [key, value] of this.groupIdToNames) {
      if (key === id) {
        return `${value}`;
      }
    }
    return '';
  }

  getStatusDevice(device: Device): string {
    for (let capability of device.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        return capability.state.value;
      }
    }
    return '';
  }

  setDeviceStatus(deviceToUpd: Device, deviceReceived: Device) {
    let newStatus = this.getStatusDevice(deviceReceived);
    for (let capability of deviceToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  setDeviceStatusInverse(deviceToUpd: Device, deviceReceived: Device) {
    let newStatus;
    if (this.getStatusDevice(deviceReceived) === 'true') {
      newStatus = 'false'
    } else {
      newStatus = 'true'
    }
    for (let capability of deviceToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  unsetErrorAndShowAiAnalyses() {
    this.isAiError = false;
    this.showAiAnalyses = false;
  }
}
