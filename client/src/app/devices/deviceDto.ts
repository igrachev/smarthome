import {Capability} from "../models/capability";

export class DeviceDto {
  id = '';
  name = '';
  state = '';
  capabilities: Capability[] = [];
}
