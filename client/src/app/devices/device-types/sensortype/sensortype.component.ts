import {Component, Input, OnInit} from "@angular/core";
import {Device} from "../../device";
import {Subscription, timer} from "rxjs";
import {SmartHomeService} from "../../../services/smart-home.service";

@Component({
  selector: "app-sensortype",
  template: `
    <div id="temp-hum-battery">
      <div *ngIf="propertiesPresent('float', 'temperature')"
           [class.low-temperature]="getValueNumber('float', 'temperature') < 23"
           [class.norm-temperature]="getValueNumber('float', 'temperature') >= 23 &&
           getValueNumber('float', 'temperature') < 26"
           [class.high-temperature]="getValueNumber('float', 'temperature') >= 26"
           class="column">
        <div class="value-number">
          {{getValueString('float', 'temperature')}}{{getUnit('float', 'temperature')}}
        </div>
        <div *ngIf="getValueNumber('float', 'temperature') < 23">
          <div class="snowflake">
            ❅
          </div>
          <div class="snowflake">
            ❅
          </div>
          <div class="snowflake">
            ❆
          </div>
          <div class="snowflake">
            ❄
          </div>
          <div class="snowflake">
            ❅
          </div>
          <div class="snowflake">
            ❆
          </div>
          <div class="snowflake">
            ❄
          </div>
          <div class="snowflake">
            ❅
          </div>
          <div class="snowflake">
            ❆
          </div>
          <div class="snowflake">
            ❄
          </div>
        </div>
        <div class="text">Temperature</div>
      </div>
      <div *ngIf="propertiesPresent('float', 'humidity')"
           class="column"
           id="humidity">
        <div class="value-number">
          {{getValueString('float', 'humidity')}}{{getUnit('float', 'humidity')}}
        </div>
        <div class="bubble small"></div>
        <div class="bubble s-medium"></div>
        <div class="bubble medium"></div>
        <div class="bubble large"></div>
        <div class="bubble small-l"></div>
        <div class="text">Humidity</div>
      </div>
      <div *ngIf="propertiesPresent('float', 'pressure')"
           class="column">
        <div class="value-number">
          {{getValueString('float', 'pressure')}}{{getUnit('float', 'pressure')}}
        </div>
        <div class="text">Pressure</div>
      </div>
      <div *ngIf="propertiesPresent('float', 'battery_level')"
           class="column">
        <div class="value-number">
          {{getValueString('float', 'battery_level')}}{{getUnit('float', 'battery_level')}}
        </div>
        <div id="battery"></div>
        <div id="battery-1"
             [class.battery-level-low]="getValueNumber('float', 'battery_level') < 20"
             [class.battery-level-medium]="getValueNumber('float', 'battery_level') >= 20 &&
             getValueNumber('float', 'battery_level') < 70"
             [class.battery-level-high]="getValueNumber('float', 'battery_level') >= 70"
        ></div>
        <div id="battery-2"
             [class.battery-level-none]="getValueNumber('float', 'battery_level') < 20"
             [class.battery-level-medium]="getValueNumber('float', 'battery_level') >= 20 &&
             getValueNumber('float', 'battery_level') < 70"
             [class.battery-level-high]="getValueNumber('float', 'battery_level') >= 70"
        ></div>
        <div id="battery-3"
             [class.battery-level-none]="getValueNumber('float', 'battery_level') < 70"
             [class.battery-level-high]="getValueNumber('float', 'battery_level') >= 70"
        ></div>
        <div id="battery-top"></div>
        <div class="text">Battery</div>
      </div>
      <div *ngIf="propertiesPresent('event', 'open')"
           class="column">
        <div class="value-text">
          {{getValueString('event', 'open')}}
        </div>
        <div id="lock"></div>
        <div class="text">Door sensor</div>
      </div>
      <div *ngIf="propertiesPresent('event', 'button')"
           class="column">
        <div class="value-text">
          {{getValueString('event', 'button')}}
        </div>
        <div class="text">Wireless switch</div>
      </div>
      <div *ngIf="propertiesPresent('event', 'motion')"
           class="column">
        <div class="value-text">
          {{getValueString('event', 'motion') | shortenString:limit}}
        </div>
        <div class="text">Motion</div>
      </div>
      <div *ngIf="propertiesPresent('float', 'illumination')"
           class="column">
        <div class="value-number">
          {{getValueString('float', 'illumination')}}{{getUnit('float', 'illumination')}}
        </div>
        <div class="text">Illumination</div>
      </div>
    </div>
  `,
  styleUrls: ["sensortype.component.css"]
})
export class SensorTypeComponent implements OnInit {
  @Input() device: Device = new Device;
  subscription!: Subscription;
  limit = 6;
  units: Map<string, string> = new Map([
    ['unit.temperature.celsius', '°C'],
    ['unit.percent', '%'],
    ['unit.illumination.lux', 'lx'],
    ['unit.pressure.mmhg', '\nmmhg']
  ]);

  constructor(private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.updateDeviceContinuously(this.device.id)
  }

  updateDeviceContinuously(id: string) {
    this.subscription = timer(1000, 10000).subscribe(
      () => {
        this.smartHomeService.retrieveDeviceById(id)
          .then(device => this.device = device);
      }
    )
  }

  propertiesPresent(type: string, parametersInstance: string): boolean {
    for (let property of this.device.properties) {
      if (property.type === `devices.properties.${type}` && property.parameters.instance === parametersInstance) {
        return true;
      }
    }
    return false;
  }

  // @ts-ignore
  getValueString(type: string, parametersInstance: string): string {
    for (let property of this.device.properties) {
      if (property.type === `devices.properties.${type}` && property.parameters.instance === parametersInstance) {
        return property.state.value;
      }
    }
  }

  getValueNumber(type: string, parametersInstance: string): number {
    let valueString = this.getValueString(type, parametersInstance);
    return parseInt(valueString);
  }

  // @ts-ignore
  getUnit(type: string, parametersInstance: string): string {
    let deviceUnit = '';
    for (let property of this.device.properties) {
      if (property.type === `devices.properties.${type}` && property.parameters.instance === parametersInstance) {
        deviceUnit = property.parameters.unit;
      }
    }
    for(let[key, value] of this.units) {
      if(key === deviceUnit) {
        return value;
      }
    }
    return '';
  }


}
