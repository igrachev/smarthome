import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {Device} from "./device";
import {SmartHomeService} from "../services/smart-home.service";
import {Subscription, timer} from "rxjs";
import {Group} from "../groups/group";

@Component({
  selector: 'app-devices',
  template: `
    <div id="wrapper1" *ngIf="(devicesFiltered.length != 0 || groupsFiltered.length != 0) && mainIsLoading === false">
      <div *ngIf="devicesFiltered.length > 2" id="input-device-name">
        <label>
          <input type="text" [(ngModel)]="filerDevice" placeholder="Search device">
        </label>
      </div>
      <div>
        <ul id="devices">
          <li [ngClass]="{'device-name-and-room':(!roomId) || (!roomId && favouriteDevicesSelected === true)}"
              *ngFor="let group of groupsFiltered | filter:filerDevice:'name'">
            <div *ngIf="(!roomId) || (!roomId && favouriteDevicesSelected === true)">
              <div id="device-name">
                <div id="device-name-0">
                  <div id="device-name-0-1">
                    <a id="device-name-1"
                       [routerLink]="['/group', group.id, this.householdId , this.householdName]"
                       [ngStyle]="{'color': group.state === 'online' || group.state === 'split' ? 'lightgrey':'darkgrey'}">
                      {{ group.name }}
                    </a>
                    <div id="device-count">{{ group.devices.length }}</div>
                  </div>
                </div>
                <div *ngIf="group.state != 'online' && group.state != 'split'" id="offline-message">
                  Offline
                </div>
              </div>
              <div id="device-name-3">
                <div class="switch" *ngIf="group.type != 'devices.types.sensor' &&
                group.type != 'devices.types.smart_speaker.yandex.station.mini' &&
                (group.state === 'online' || group.state === 'split')">
                  <label>
                    <input type="checkbox" [checked]="getStatusGroup(group) === 'true'"
                           (change)="turnOnOffGroup(group.id, getStatusGroup(group))">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
          </li>
          <li class="device-name-and-room" *ngFor="let device of devicesFiltered | filter:filerDevice:'name'">
            <div id="device-name">
              <div id="device-name-0">
                <div>
                  <a id="device-name-1"
                     [routerLink]="['/device', device.id, this.householdId , this.householdName]"
                     [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">
                    {{ device.name }}
                  </a>
                </div>
              </div>
              <div *ngIf="device.state != 'online'" id="offline-message">
                Offline
              </div>
            </div>
            <div id="room-name-and-switch">
              <div id="room-name" *ngIf="!roomId"
                   [ngStyle]="{'color': device.state === 'online' ? 'lightgrey':'darkgrey'}">
                {{ getRoomName(device.room) }}
              </div>
              <div [ngClass]="{'ai-controlled': device.smartScenarioNames.length != 0}">
                <div *ngIf="device.smartScenarioNames.length != 0" class="ai-controlled-text"
                (mouseenter)="aiHoverIn(device.id)" (mouseleave)="aiHoverOut()">
                  AI
                </div>
                <div *ngIf="hoveredDeviceId === device.id" id="show-smart-scenarios">
                  Controlled by smart scenarios:
                  <li *ngFor="let smartScenario of device.smartScenarioNames">
                    {{ smartScenario }}
                  </li>
                </div>
                <div class="switch" *ngIf="device.type != 'devices.types.sensor' &&
                device.type != 'devices.types.smart_speaker.yandex.station.mini' &&
                device.state === 'online'">
                  <label>
                    <input type="checkbox" [checked]="getStatusDevice(device) === 'true'"
                           (change)="turnOnOffDevice(device)">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div id="arrow-up" (click)="navigateUp()">⬆</div>
    </div>
    <div *ngIf="mainIsLoading === true" class="loading-outside">
      <div class="loading-inside">
        <div class="wifi-outside">
          <div class="wifi"></div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./devices.component.css"]
})

export class DevicesComponent implements OnChanges, OnInit, OnDestroy {
  @Output() devicesIsloading = new EventEmitter<boolean>();
  @Output() deviceTypes = new EventEmitter<Set<string>>();
  @Output() error = new EventEmitter<string>();
  @Input() mainIsLoading?: boolean;
  @Input() householdName?: string;
  @Input() householdId?: string;
  @Input() roomId?: string;
  @Input() roomIdToNameMap?: Map<string, string>;
  @Input() deviceType?: string;
  @Input() favouriteDevicesSelected?: boolean;
  devices: Device[] = [];
  devicesFiltered: Device[] = [];
  groups: Group[] = [];
  groupsFiltered: Group[] = [];
  subscription!: Subscription;
  deviceTypesSet: Set<string> = new Set<string>();
  renewDeviceTypesSet = false;
  filerDevice = '';
  hoveredDeviceId?: string;

  constructor(private smartHomeService: SmartHomeService) {
  }

  aiHoverIn(deviceId: string) {
    this.hoveredDeviceId = deviceId;
  }

  aiHoverOut() {
    this.hoveredDeviceId = undefined;
  }

  navigateUp() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  ngOnInit(): void {
    this.mainIsLoading = true;
    this.devicesIsloading.emit(true);
    console.info(`devices ngOninit Started, id ${this.householdId} and name ${this.householdName}`);
    this.smartHomeService.retrieveAllDevices()
      .then(devices => this.devices = devices)
      .then(devices => this.devicesFiltered = devices
        .filter(device => device.household_id === this.householdId && device.groups.length === 0)
        .sort((device1, device2) => device1.name.localeCompare(device2.name)))
      .then(() => this.getAllGroups())
      .then(() => this.devicesIsloading.emit(false))
      .then(() => this.getDeviceTypes(this.renewDeviceTypesSet))
      .then(() => this.updateDeviceAndGroupStatusContinuously())
      .catch(error => this.error.emit(error.message));
  }

  getAllGroups() {
    this.smartHomeService.retrieveAllGroups()
      .then(groups => this.groups = groups)
      .then(groups => this.groupsFiltered = groups
        .filter(group => group.household_id === this.householdId)
        .sort((group1, group2) => group1.name.localeCompare(group2.name)))
      .catch(error => this.error.emit(error.message));
  }

  getDeviceTypes(clearNeeded: boolean): void {
    if (clearNeeded) {
      this.deviceTypesSet.clear();
    }
    for (let device of this.devicesFiltered) {
      this.deviceTypesSet.add(device.type);
    }
    this.deviceTypes.emit(this.deviceTypesSet);
  }

  updateDeviceAndGroupStatusContinuously(): void {
    this.subscription = timer(1000, 10000).subscribe(
      () => {
        this.devicesFiltered.forEach(device => {
            this.smartHomeService.retrieveDeviceById(device.id)
              .then(receivedDevice => this.setDeviceStatus(device, receivedDevice));
          }
        );
        this.groupsFiltered.forEach(group => {
          this.smartHomeService.retrieveGroupById(group.id)
            .then(receivedGroup => this.setGroupStatus(group, receivedGroup));
        })
      }
    )
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.info('ngOnChanges devices started');
    this.renewDeviceTypesSet = this.renewDeviceTypesSetNeeded();
    this.devicesFiltered = this.devices
      .filter(device => device.household_id === this.householdId && device.groups.length === 0)
      .filter(device => this.roomId ? device.room === this.roomId : true)
      .filter(device => (this.deviceType && this.deviceType != 'allActiveDevices') ? device.type === this.deviceType : true)
      .filter(device => (this.deviceType === 'allActiveDevices') ? this.getStatusDevice(device) === 'true' : true)
      .filter(device => (this.favouriteDevicesSelected === true) ? device.favourite : true)
      .sort((device1, device2) => device1.name.localeCompare(device2.name));

    this.groupsFiltered = this.groups
      .filter(group => group.household_id === this.householdId)
      .filter(group => (this.deviceType && this.deviceType != 'allActiveDevices') ? group.type === this.deviceType : true)
      .filter(group => (this.deviceType === 'allActiveDevices') ? this.getStatusGroup(group) === 'true' : true)
      .filter(group => (this.favouriteDevicesSelected === true) ? group.favourite === true : true)
      .sort((group1, group2) => group1.name.localeCompare(group2.name));

    this.getDeviceTypes(this.renewDeviceTypesSet);
  }


  renewDeviceTypesSetNeeded(): boolean {
    if (this.devicesFiltered.length != 0 && this.devicesFiltered[0].household_id != this.householdId) {
      return true;
    } else {
      return false;
    }
  }

  getRoomName(roomId: string): string {
    // @ts-ignore
    for (let [key, value] of this.roomIdToNameMap) {
      if (key === roomId) {
        return `${value}`;
      }
    }
    return '';
  }

  turnOnOffDevice(device: Device): void {
    this.smartHomeService.turnOnOffDevice(device.id, this.getStatusDevice(device));
  }

  turnOnOffGroup(id: string, status: string) {
    this.smartHomeService.turnOnOffGroup(id, status);
  }

  getStatusGroup(group: Group): string {
    for (let capability of group.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        return capability.state.value;
      }
    }
    return '';
  }

  setGroupStatus(groupToUpd: Group, groupReceived: Group) {
    let newStatus = this.getStatusGroup(groupReceived);
    for (let capability of groupToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  getStatusDevice(device: Device): string {
    for (let capability of device.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        return capability.state.value;
      }
    }
    return '';
  }

  setDeviceStatus(deviceToUpd: Device, deviceReceived: Device) {
    let newStatus = this.getStatusDevice(deviceReceived);
    for (let capability of deviceToUpd.capabilities) {
      if (capability.type === 'devices.capabilities.on_off' && capability.state) {
        capability.state.value = newStatus;
      }
    }
  }

  ngOnDestroy(): void {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
