import {Capability} from "../models/capability";
import {Property} from "../models/property";

export class Device {
  id = '';
  name = '';
  aliases: string[] = [];
  groups: string[] = [];
  household_id = '';
  external_id = '';
  skill_id = '';
  room = '';
  type = '';
  state = '';
  smartScenarioNames: string[] = [];
  favourite?: boolean;
  capabilities: Capability[] = [];
  properties: Property[] = [];
}
