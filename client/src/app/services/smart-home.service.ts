import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Household} from "../households/household";
import {firstValueFrom} from "rxjs";
import {Device} from "../devices/device";
import {Room} from "../rooms/room";
import {Scenario} from "../scenarios/scenario";
import {DeviceDto} from "../devices/deviceDto";
import {Group} from "../groups/group";
import {environment} from "../../environments/environment";
import {Authresponse} from "../models/authresponse";
import {Chatresponse} from "../models/chatresponse";
import {SmartScenario} from "../models/smartscenario";

@Injectable({
  providedIn: 'root'
})
export class SmartHomeService {
  private static BASE_URI = environment.apiUrl;


  token?: string;

  constructor(private httpClient: HttpClient) {
  }

  async authenticate() {
    if (!this.token) {
      const response = await firstValueFrom(
        this.httpClient.post<Authresponse>(`${SmartHomeService.BASE_URI}/auth/authenticate`,
          {
            "username": `${environment.authUsername}`,
            "password": `${environment.authPassword}`
          }
        )
      );
      return this.token = response.token;
    } else {
      return Promise.resolve();
    }
  }

  async retrieveAllGroups(): Promise<Group[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Group[]>(`${SmartHomeService.BASE_URI}/resources/groups`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveGroupById(id: string): Promise<Group> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Group>(`${SmartHomeService.BASE_URI}/resources/groups/${id}`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveAllHouseholds(): Promise<Household[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Household[]>(
        `${SmartHomeService.BASE_URI}/resources/households`, {
          headers: {'Authorization': `Bearer ${this.token}`}
        }
      )
    );
  }

  async retrieveAllDevices(): Promise<Device[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Device[]>(`${SmartHomeService.BASE_URI}/resources/devices`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveDeviceDto(id: string): Promise<DeviceDto> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<DeviceDto>(`${SmartHomeService.BASE_URI}/resources/devices/dto/${id}`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveDeviceById(id: string): Promise<Device> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Device>(`${SmartHomeService.BASE_URI}/resources/devices/${id}`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveRoomById(id: string): Promise<Room> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Room>(`${SmartHomeService.BASE_URI}/resources/rooms/${id}`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveAllRooms(): Promise<Room[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient
        .get<Room[]>(`${SmartHomeService.BASE_URI}/resources/rooms`,
          {
            headers: {'Authorization': `Bearer ${this.token}`}
          }
        )
    );
  }

  async retrieveAllRoomsByHouseholdId(householdId: string): Promise<Room[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient
        .get<Room[]>(`${SmartHomeService.BASE_URI}/resources/rooms/all`,
          {
            params: {
              householdId: householdId
            },
            headers: {'Authorization': `Bearer ${this.token}`}
          }
        )
    );
  }

  async retrieveAllScenarios(): Promise<Scenario[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Scenario[]>(`${SmartHomeService.BASE_URI}/resources/scenarios`,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async sendMessageToChat(householdId: string, message: string): Promise<Chatresponse> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Chatresponse>(`${SmartHomeService.BASE_URI}/chat/generate`,
        {
          params: {
            householdId: householdId,
            message: message
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async getAiAnalysesForDevice(householdId: string, deviceId: string): Promise<Chatresponse> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Chatresponse>(`${SmartHomeService.BASE_URI}/chat/analyse/device`,
        {
          params: {
            householdId: householdId,
            deviceId: deviceId
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async getAiAnalysesForGroup(householdId: string, groupId: string): Promise<Chatresponse> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<Chatresponse>(`${SmartHomeService.BASE_URI}/chat/analyse/group`,
        {
          params: {
            householdId: householdId,
            groupId: groupId
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async retrieveAllSmartScenarios(householdId: string): Promise<SmartScenario[]> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.get<SmartScenario[]>(`${SmartHomeService.BASE_URI}/smartscenarios`,
        {
          params: {
            householdId: householdId,
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async activateSmartScenario(smartScenarioId: number): Promise<SmartScenario> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post<SmartScenario>(`${SmartHomeService.BASE_URI}/smartscenarios/activate`,
        null,
        {
          params: {
            smartScenarioId: smartScenarioId,
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async startSmartScenario(smartScenarioId: number): Promise<SmartScenario> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post<SmartScenario>(`${SmartHomeService.BASE_URI}/smartscenarios/start`,
        null,
        {
          params: {
            smartScenarioId: smartScenarioId,
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async createSmartScenario(householdId: string, roomId: string, promt: string): Promise<SmartScenario> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post<SmartScenario>(`${SmartHomeService.BASE_URI}/smartscenarios/create`,
        null,
        {
          params: {
            householdId: householdId,
            roomId: roomId,
            promt: promt
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async deactivateSmartScenario(smartScenarioId: number): Promise<SmartScenario> {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post<SmartScenario>(`${SmartHomeService.BASE_URI}/smartscenarios/deactivate`,
        null,
        {
          params: {
            smartScenarioId: smartScenarioId,
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async deleteSmartScenario(smartScenarioId: number) {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.delete(`${SmartHomeService.BASE_URI}/smartscenarios/delete`,
        {
          params: {
            smartScenarioId: smartScenarioId,
          },
          headers: {'Authorization': `Bearer ${this.token}`}
        })
    );
  }

  async turnOnOffDevice(id: string, status: string) {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post(`${SmartHomeService.BASE_URI}/resources/devices/turnonoff`,
        {
          "devices": [
            {
              "id": id,
              "actions": [
                {
                  "type": "devices.capabilities.on_off",
                  "state": {
                    "instance": "on",
                    "value": status !== 'true'
                  }
                }
              ]
            }
          ]
        },
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        }
      )
    );
  }

  async turnOnOffGroup(id: string, status: string) {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post(`${SmartHomeService.BASE_URI}/resources/groups/turnonoff/${id}`,
        {
          "actions": [
            {
              "type": "devices.capabilities.on_off",
              "state": {
                "instance": "on",
                "value": status !== 'true'
              }
            }
          ]
        },
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        }
      )
    );
  }

  async activateScenario(id: string) {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.post(`${SmartHomeService.BASE_URI}/resources/scenarios/activate/${id}`,
        null,
        {
          headers: {'Authorization': `Bearer ${this.token}`}
        }
      )
    );
  }

  async updateDevice(id: string, path: string, value: string) {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.patch(`${SmartHomeService.BASE_URI}/resources/devices/${id}`,
        [
          {
            "op": "replace",
            "path": path,
            "value": value
          }
        ],
        {
          headers: {
            'Content-Type': 'application/json-patch+json',
            'Authorization': `Bearer ${this.token}`
          }
        }
      )
    );
  }

  async updateGroup(id: string, path: string, value: string) {
    await this.authenticate();
    return await firstValueFrom(
      this.httpClient.patch(`${SmartHomeService.BASE_URI}/resources/groups/${id}`,
        [
          {
            "op": "replace",
            "path": path,
            "value": value
          }
        ],
        {
          headers: {
            'Content-Type': 'application/json-patch+json',
            'Authorization': `Bearer ${this.token}`
          }
        }
      )
    );
  }
}
