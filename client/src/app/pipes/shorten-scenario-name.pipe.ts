import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shortenScenarioName'
})
export class ShortenScenarioNamePipe implements PipeTransform {

  transform(scenarioName: string, isActive: string): string {
    if(isActive === 'false' && scenarioName.length > 16) {
      return `${scenarioName.substring(0, 16)}...`;
    }
    if (isActive === 'true' && scenarioName.length > 20) {
      return `${scenarioName.substring(0, 20)}...`;
    }
    return scenarioName;
  }

}
