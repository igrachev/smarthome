import {Pipe, PipeTransform} from '@angular/core';
import {Scenario} from "../scenarios/scenario";

@Pipe({
  name: 'filterScenario'
})
export class FilterScenarioPipe implements PipeTransform {

  transform(scenarios: Scenario[], limit: number): Scenario[] {
    if (scenarios.length < limit) {
      return scenarios;
    }
    let scenariosShortened: Scenario[] = [];
    scenarios.forEach((value, index) => {
      if(index < limit) {
        scenariosShortened.push(value);
      }
    });
    return scenariosShortened;
  }

}


