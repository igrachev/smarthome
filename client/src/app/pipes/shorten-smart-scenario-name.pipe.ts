import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortenSmartScenarioName'
})
export class ShortenSmartScenarioNamePipe implements PipeTransform {

  transform(smartScenarioName: string): string {
    if(smartScenarioName.length > 44) {
      return `${smartScenarioName.substring(0, 42)}...`;
    }
    return smartScenarioName;
  }

}
