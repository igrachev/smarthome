import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterDevicePipe implements PipeTransform {

  constructor() {
  }

  transform(value: any, filterDevice: string, propName: string): any {
    if(value.length === 0 || filterDevice === '') {
      return value;
    }
    const resultArray = [];
    for (let item of value) {
      if(item[propName].toLowerCase().startsWith(filterDevice.trim().toLowerCase())) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }


}
