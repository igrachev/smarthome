import {Pipe, PipeTransform} from '@angular/core';
import {Scenario} from "../scenarios/scenario";

@Pipe({
  name: 'filterScenarioName'
})
export class FilterScenarioNamePipe implements PipeTransform {

  transform(scenarios: Scenario[], value: string): Scenario[] {
    if(scenarios.length === 0 || value === '') {
      return scenarios;
    }
    let scenariosMetValue: Scenario[] = [];
    for(let scenario of scenarios) {
      if(scenario.name.toLowerCase().startsWith(value.trim().toLowerCase())) {
        scenariosMetValue.push(scenario);
      }
    }
    return scenariosMetValue;
  }

}
