import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {SmartHomeService} from "../services/smart-home.service";
import {Room} from "./room";

@Component({
  selector: 'app-rooms',
  template: `
      <div *ngIf="mainIsLoading === false">
          <ul id="rooms">
              <li id="favourite" (click)="selectFavourite()" [ngStyle]="{'background': favouritesSelected ? 'black':'#464956'}">Favourite</li>
              <li id="all-rooms" (click)="clearRoomFilter()" [ngStyle]="{'background': allRoomsSelected ? 'black':'#464956'}">All rooms</li>
              <li *ngFor="let room of roomsFiltered" (click)="selectRoom(room)" [ngStyle]="{'background': room.selected ? 'black':'#464956'}">{{room.name}}</li>
          </ul>
      </div>
  `,
  styleUrls: ["./rooms.component.css"]

})
export class RoomsComponent implements OnInit, OnChanges {
  @Input() householdId?: string;
  @Input() mainIsLoading?: boolean;
  @Output() error = new EventEmitter<string>();
  @Output() roomSelected = new EventEmitter<Room>();
  @Output() favouriteSelected = new EventEmitter<boolean>();
  @Output() roomIdToNameMap = new EventEmitter<Map<string, string>>();
  @Output() roomsIsloading = new EventEmitter<boolean>();
  map = new Map;
  rooms: Room[] = [];
  roomsFiltered: Room[] = [];
  allRoomsSelected = true;
  favouritesSelected = false;

  constructor(private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.roomsIsloading.emit(true);
    this.smartHomeService.retrieveAllRooms()
      .then(rooms => this.rooms = rooms)
      .then(rooms => this.roomsFiltered = rooms.filter(
        room => room.household_id === this.householdId
      ))
      .then(rooms => rooms.sort(
        (room1, room2) => room1.name.localeCompare(room2.name)
      ))
      .then(rooms => rooms.forEach(room => this.map.set(room.id, room.name)))
      .then(() => this.roomIdToNameMap.emit(this.map))
      .then(() => this.roomsIsloading.emit(false))
      .catch(error => this.error.emit(error.message));
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.info('rooms onChanges()')
    this.roomsFiltered = this.rooms
      .filter(room => room.household_id === this.householdId)
      .sort((room1, room2) => room1.name.localeCompare(room2.name));

    this.roomsFiltered.forEach(room => this.map.set(room.id, room.name));
    this.roomIdToNameMap.emit(this.map);
  }

  selectRoom(room: Room): void {
    this.setRoomSelected(room);
    this.allRoomsSelected = false;
    this.favouritesSelected = false;
    this.roomSelected.emit(room);
  }

  setRoomSelected(room: Room): void {
    for(let roomm of this.rooms) {
      if(roomm.id === room.id) {
        roomm.selected = true;
      } else {
        roomm.selected = false;
      }
    }
  }

  clearRoomFilter() {
    this.setAllRoomsUnselected();
    this.favouritesSelected = false;
    this.allRoomsSelected = true;
    this.roomSelected.emit();
  }

  setAllRoomsUnselected() {
    for(let room of this.rooms) {
      room.selected = false;
    }
  }

  selectFavourite() {
    this.setAllRoomsUnselected();
    this.allRoomsSelected = false;
    this.favouritesSelected = true;
    this.favouriteSelected.emit(true);
  }
}
