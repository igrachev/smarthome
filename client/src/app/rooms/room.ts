export class Room {
  id = '';
  name = '';
  household_id = '';
  devices: string[] = [];
  selected = false;
  clicked = false;
}
