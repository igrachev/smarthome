import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SmartHomeService} from "../../services/smart-home.service";
import {Room} from "../../rooms/room";
import {Device} from "../../devices/device";

@Component({
  selector: 'app-household',
  template: `
    <div id="wrapper1">
      <div class="displayed"></div>
      <div id="wrapper2" *ngIf="isLoading === false && !error">
        <p id="householdName">{{householdName }}</p>
        <p id="rooms">Rooms</p>
        <ul>
          <li *ngFor="let room of roomsFiltered" (click)="getRoomClicked(room)">
            <div id="roomNameAndDeviceCount">
              {{getRoomName(room.id)}}
              <p id="deviceCount">Devices: {{getDeviceCount(room.id)}}</p>
              <div id="devices" *ngIf="room.clicked">
                <ul>
                  <li id="device" *ngFor="let device of getDevicesOfRoom(room.id)" [routerLink]="['/device', device.id, this.householdId, this.householdName]">{{device.name}}</li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div *ngIf="isLoading === true && !error" class="wifi-outside">
        <div class="wifi-inside">
          <div class="wifi"></div>
        </div>
      </div>
      <div *ngIf="error" id="error">
        <div id="error-content">
          <h2 id="error-title">Error occured</h2>
          <p id="error-message">{{error}}</p>
          <button id="error-button" (click)="onHandleError()">Back to start page</button>
        </div>
      </div>
      <div class="displayed"></div>
    </div>
  `,
  styleUrls: ["./household.component.css"]
})
export class HouseholdComponent implements OnInit, OnChanges {
  householdId?: string;
  householdName?: string;
  devices: Device[] = [];
  devicesFiltered: Device[] = [];
  rooms: Room[] = [];
  roomsFiltered: Room[] = [];
  roomIdToNameMap: Map<string, string> = new Map<string, string>();
  isLoading?: boolean;
  error?: string;

  constructor(private route: Router, private activatedRoute: ActivatedRoute, private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.activatedRoute.params.subscribe(
      params => {
        this.householdId = params['id'];
        this.householdName = params['name'];
      }
    );
    this.smartHomeService.retrieveAllRooms()
      .then(rooms => this.rooms = rooms)
      .then(rooms => this.roomsFiltered = rooms.filter(
        room => room.household_id === this.householdId
      ))
      .then(rooms => rooms.sort(
        (room1, room2) => room1.name.localeCompare(room2.name)
      ))
      .then(rooms => rooms.forEach(room => this.roomIdToNameMap.set(room.id, room.name)))
      .catch(error => this.error = error.message)
      .then(() => {
        this.smartHomeService.retrieveAllDevices()
          .then(devices => this.devices = devices)
          .then(devices => this.devicesFiltered = devices
            .filter(device => device.household_id === this.householdId))
          .then(() => this.isLoading = false)
          .catch(error => this.error = error.message);
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  getRoomName(roomId: string): string {
    // @ts-ignore
    for (let [key, value] of this.roomIdToNameMap) {
      if (key === roomId) {
        return `${value}`;
      }
    }
    return '';
  }

  getDeviceCount(roomId: string): number {
    return this.devicesFiltered
      .filter(device => device.room === roomId)
      .length;
  }

  getDevicesOfRoom(roomId: string) {
    return this.devicesFiltered
      .filter(device => device.room === roomId);
  }

  getRoomClicked(room: Room): void {
    room.clicked = !room.clicked;
  }

  onHandleError() {
    this.error = undefined;
    this.route.navigate(['main', this.householdId, this.householdName]);
  }

}
