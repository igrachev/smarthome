import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Household} from "./household";
import {SmartHomeService} from "../services/smart-home.service";

@Component({
  selector: 'app-households',
  template: `
    <div id="wrapper-1">
      <div id="wrapper-2">
        <div id="house">
          <div>
            <a id="household-name" [routerLink]="['/main', householdId, householdName]">
              {{ householdName }}
            </a>
            <div id="temp-and-humidity">
              <div *ngIf="temperature">{{ temperature }}°</div>
              <div *ngIf="humidity" id="humidity">{{ humidity }}
                <span id="humidity-sign">💧</span>
              </div>
            </div>
          </div>
          <div id="arrow" (click)="expandMoreInfo()">⤵</div>
        </div>
        <div *ngIf="showMoreInfo" id="more-info">
          <a id="householdNameLink" [routerLink]="['/household', householdId, householdName]"
             (click)="hideMoreInfo()">
            More about {{ householdName }}
          </a>
        </div>
      </div>
      <div id="wrapper-3">
        <select
          (change)="selectHouseholdAndSetTemperatureAndHumidity($any($event.target).value)">
          <option value="" disabled selected>Select home</option>
          <option *ngFor="let household of households; let i = index"
                  [value]="i"
          >{{ household.name }}
          </option>
        </select>
      </div>
    </div>
  `,
  styleUrls: ["./households.component.css"]
})

export class HouseholdsComponent implements OnInit, OnDestroy {
  @Output() select = new EventEmitter<Household>();
  @Output() error = new EventEmitter<string>();
  temperature?: string;
  humidity?: string;
  households: Household[] = [];
  householdId?: string;
  householdName?: string;
  showMoreInfo = false;

  constructor(private smartHomeService: SmartHomeService) {
  }

  ngOnInit(): void {
    console.info(`household onInit`);
    this.smartHomeService.retrieveAllHouseholds()
      .then(households => this.households = households)
      .then(() => this.selectHouseholdAndSetTemperatureAndHumidity(0))
      .catch(error => this.error.emit(error.message));
  }

  selectHouseholdAndSetTemperatureAndHumidity(householdIndex: number): void {
    this.householdName = this.households[householdIndex].name;
    this.householdId = this.households[householdIndex].id;
    this.getHouseholdsTemperatureAndHumidity();
    this.select.emit(this.households[householdIndex]);
  }

  getHouseholdsTemperatureAndHumidity(): void {
    let temperaturePresent = false;
    let humidityPresent = false;
    this.smartHomeService.retrieveAllDevices()
      .then(devices => devices.filter(device => device.household_id === this.householdId))
      .then(devices => devices.filter(device => device.properties != null && device.properties.length != 0))
      .then(devices => devices.filter(device => {
            console.log(`${device.name}`)
            for (let property of device.properties) {
              if (property.type === `devices.properties.float` && property.parameters.instance === `temperature` && property.state != null) {
                this.temperature = property.state.value;
                console.log(`temp is ${this.temperature}`);
                temperaturePresent = true;
              }
              if (property.type === `devices.properties.float` && property.parameters.instance === `humidity` && property.state != null) {
                this.humidity = property.state.value;
                console.log(`hum is ${this.humidity}`);
                humidityPresent = true;
              }
            }
          }
        )
      )
      .then(() => {
        if (!temperaturePresent) {
          this.temperature = undefined;
          console.log(`temp is ${this.temperature}`);
        }
        if (!humidityPresent) {
          this.humidity = undefined;
          console.log(`hum is ${this.humidity}`);
        }
      })
  }

  expandMoreInfo() {
    this.showMoreInfo = !this.showMoreInfo;
  }

  hideMoreInfo() {
    this.showMoreInfo = false;
  }

  ngOnDestroy(): void {
    this.temperature = undefined;
    this.humidity = undefined;
  }

}
