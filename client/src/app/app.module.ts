import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {DevicesComponent} from './devices/devices.component';
import {HouseholdsComponent} from './households/households.component';
import {RoomsComponent} from './rooms/rooms.component';
import {ScenarioComponent} from './scenarios/scenario.component';
import {FormsModule} from "@angular/forms";
import {FilterDevicePipe} from './pipes/filter-device.pipe';
import {DeviceTypesComponent} from './devicetypes/device-types.component';
import {MainComponent} from './main/main.component';
import {RouterModule} from "@angular/router";
import {DeviceComponent} from './devices/device/device.component';
import {HomeComponent} from './home/home.component';
import {HouseholdComponent} from './households/household/household.component';
import {GroupComponent} from './groups/group.component';
import {FilterScenarioPipe} from './pipes/filter-scenario.pipe';
import {AllscenariosComponent} from './scenarios/all-scenarios/allscenarios.component';
import {ShortenScenarioNamePipe} from './pipes/shorten-scenario-name.pipe';
import {FilterScenarioNamePipe} from './pipes/filter-scenario-name.pipe';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule} from "@angular/material/tabs";
import {SensorTypeComponent} from "./devices/device-types/sensortype/sensortype.component";
import {ShortenStringPipe} from "./pipes/shorten-string";
import { ChatComponent } from './chat/chat.component';
import { SmartScenarioDashboardComponent } from './smartscenario/smartscenariodashboard/smart-scenario-dashboard.component';
import { SmartScenarioComponent } from './smartscenario/smartscenariocomponent/smart-scenario.component';
import { ShortenSmartScenarioNamePipe } from './pipes/shorten-smart-scenario-name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DevicesComponent,
    HouseholdsComponent,
    RoomsComponent,
    ScenarioComponent,
    FilterDevicePipe,
    DeviceTypesComponent,
    MainComponent,
    DeviceComponent,
    SensorTypeComponent,
    HomeComponent,
    HouseholdComponent,
    GroupComponent,
    FilterScenarioPipe,
    AllscenariosComponent,
    ShortenScenarioNamePipe,
    FilterScenarioNamePipe,
    ShortenStringPipe,
    ChatComponent,
    SmartScenarioDashboardComponent,
    SmartScenarioComponent,
    ShortenSmartScenarioNamePipe,
    ShortenSmartScenarioNamePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MatTabsModule,
    RouterModule.forRoot([
        {path: "", component: HomeComponent, children: [
            {path: "main/:id/:name", component: MainComponent},
            {path: "device/:id/:householdid/:householdname", component: DeviceComponent},
            {path: "household/:id/:name", component: HouseholdComponent},
            {path: "group/:id/:householdid/:householdname", component: GroupComponent},
            {path: "scenarios/:householdid/:householdname", component: AllscenariosComponent},
            {path: "smart-scenarios/:householdid", component: SmartScenarioComponent}
          ]},
      ]
    ),
    NoopAnimationsModule
  ],
  providers: [DevicesComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
