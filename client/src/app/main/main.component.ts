import {Component, Input, OnInit} from '@angular/core';
import {Room} from "../rooms/room";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription, timer} from "rxjs";

@Component({
  selector: 'app-main',
  template: `
    <div id="wrapper1" *ngIf="!error">
      <div id="wrapper2">
        <main>
          <div id="rooms">
            <app-rooms
              [mainIsLoading]="mainIsLoading"
              [householdId]="householdId"
              (roomSelected)="setRoomId($event)"
              (favouriteSelected)="setFavouriteDevicesSelected()"
              (roomIdToNameMap)="setMapRoomIdToName($event)"
              (error)="setError($event)"
              (roomsIsloading)="setRoomsIsLoading($event)"
            ></app-rooms>
          </div>
          <div id="deviceTypes">
            <app-device-types
              *ngIf="isDeviceTypesDisplayed"
              [mainIsLoading]="mainIsLoading"
              [deviceTypes]="deviceTypesSet"
              (typeSelected)="setDeviceType($event)"
            ></app-device-types>
          </div>
          <app-devices
            [mainIsLoading]="mainIsLoading"
            [householdName]="householdName"
            [householdId]="householdId"
            [roomId]="roomId"
            [roomIdToNameMap]="roomIdToNameMap"
            [deviceType]="deviceType"
            [favouriteDevicesSelected]="favouriteDevicesSelected"
            (deviceTypes)="setDeviceTypes($event)"
            (error)="setError($event)"
            (devicesIsloading)="setDevicesIsLoading($event)"
          ></app-devices>
        </main>
      </div>
      <aside>
        <app-scenario *ngIf="!chatOpened"
                      [mainIsLoading]="mainIsLoading"
                      [householdName]="householdName"
                      [householdId]="householdId"
                      (error)="setError($event)"
                      (scenariosAreLoading)="setScenariosAreLoading($event)"
        ></app-scenario>
        <app-chat *ngIf="chatOpened"
                  [mainIsLoading]="mainIsLoading"
                  [householdId]="householdId"
                  (chatOpened)="setChatOpened($event)"
        ></app-chat>
      </aside>
      <div *ngIf="!mainIsLoading && !chatOpened" id="chat-button" (click)="chatOpened= !chatOpened">
        AI Assistant 💬
      </div>
    </div>
    <div *ngIf="error" id="error-content">
      <h2 id="error-title">Error occured</h2>
      <p id="error-message">{{ error }}</p>
      <button id="error-button" (click)="reload()">Reload page</button>
    </div>
  `,
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {

  householdName?: string;
  householdId?: string;
  roomId?: string;
  roomIdToNameMap?: Map<string, string>;
  deviceTypesSet?: Set<string> = new Set<string>();
  deviceType?: string;
  favouriteDevicesSelected = false;
  isDeviceTypesDisplayed = true;
  error?: string;
  subscription!: Subscription;
  mainIsLoading = true;
  devicesAreLoading?: boolean;
  roomsAreLoading?: boolean;
  scenariosAreLoading?: boolean;
  @Input()
  chatOpened = false;

  constructor(private activatedRoute: ActivatedRoute, private route: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.householdId = params['id'];
        this.householdName = params['name'];
        this.roomId = undefined;
        this.deviceType = undefined;
        this.deviceTypesSet = undefined;
        this.roomIdToNameMap = undefined;
        this.continuouslyUpdateLoadingStatus();
      }
    )
  }

  setDevicesIsLoading(isLoading: boolean) {
    this.devicesAreLoading = isLoading;
  }

  setRoomsIsLoading(isLoading: boolean) {
    this.roomsAreLoading = isLoading;
  }

  setScenariosAreLoading(isLoading: boolean) {
    this.scenariosAreLoading = isLoading;
  }

  continuouslyUpdateLoadingStatus() {
    this.subscription = timer(1000, 1000).subscribe(
      () => {
        if (this.roomsAreLoading === false && this.devicesAreLoading === false && this.scenariosAreLoading === false) {
          this.mainIsLoading = false;
          this.subscription.unsubscribe();
        }
      }
    );
  }

  setRoomId(room: Room) {
    if (room) {
      this.roomId = room.id;
      this.isDeviceTypesDisplayed = false;
      this.deviceType = undefined;
      this.favouriteDevicesSelected = false;
    } else {
      this.roomId = undefined;
      this.deviceType = undefined;
      this.favouriteDevicesSelected = false;
      this.isDeviceTypesDisplayed = true;
    }
  }

  setMapRoomIdToName(map: Map<string, string>) {
    this.roomIdToNameMap = map;
  }

  setDeviceTypes(set: Set<string>) {
    this.deviceTypesSet = set;
  }

  setDeviceType(type: string) {
    this.deviceType = type;
  }

  setFavouriteDevicesSelected() {
    this.isDeviceTypesDisplayed = false;
    this.favouriteDevicesSelected = true;
    this.roomId = undefined;
    this.deviceType = undefined;
  }

  setError(error: string) {
    this.error = error;
  }

  reload() {
    this.error = undefined;
    this.route.navigate(['main', this.householdId, this.householdName]);
  }

  setChatOpened(isChatOpened: boolean) {
    this.chatOpened = isChatOpened;
  }
}
